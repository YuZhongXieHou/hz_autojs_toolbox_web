import {
    getContext,
    getEditorType,
    handlerAppByCacheChange,
    handlerByFileChange,
    initFileEditor,
    queryCacheData
} from "./../../../utils/utils.js";

let template = '<div></div>';
$.ajax({
    url: "/module/index/template/fileManage.html",
    type: 'get',
    async: false,
    success: function (res) {
        template = String(res);
    }
});
let defaultProjectJSON = {
    "abis": [
        "arm64-v8a",
        "armeabi-v7a"
    ],
    "assets": [],
    "build": {
        "build_id": "604CCB7A-25",
        "build_number": 25,
        "build_time": 1678719855559,
        "release": true
    },
    "encryptLevel": 0,
    "features": {
        "activityIntentTasks": false,
        "builtInOCR": "with-models",
        "nodejs": "enabled"
    },
    "icon": "images/ic_app_logo.png",
    "launchConfig": {
        "displaySplash": true,
        "hideLogs": true,
        "splashIcon": "images/ic_splash.png",
        "splashText": "欢迎关注华仔部落！",
        "stableMode": false
    },
    "main": "main.js",
    "name": "华仔AutoJs工具箱",
    "autoOpen":false,
    "optimization": {
        "obfuscateComponents": false,
        "removeAccessibilityService": false,
        "removeOpenCv": false,
        "unusedResources": false
    },
    "packageName": "com.zjh336.cn.tools",
    "permissionConfig": {
        "manifestPermissions": [
            "android.permission.ACCESS_WIFI_STATE",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.READ_EXTERNAL_STORAGE",
            "com.android.launcher.permission.INSTALL_SHORTCUT",
            "com.android.launcher.permission.UNINSTALL_SHORTCUT",
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.SYSTEM_ALERT_WINDOW",
            "android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS",
            "android.permission.RECEIVE_BOOT_COMPLETED",
            "android.permission.FOREGROUND_SERVICE",
            "android.permission.FORCE_STOP_PACKAGES",
            "android.permission.PACKAGE_USAGE_STATS",
            "android.permission.WRITE_SECURE_SETTINGS",
            "android.permission.WRITE_SETTINGS",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.RECORD_AUDIO",
            "com.android.alarm.permission.SET_ALARM",
            "android.permission.ACCESS_CHECKIN_PROPERTIES",
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS",
            "android.permission.ACCESS_SURFACE_FLINGER",
            "android.permission.ACCOUNT_MANAGER",
            "android.permission.AUTHENTICATE_ACCOUNTS",
            "android.permission.BATTERY_STATS",
            "android.permission.BIND_APPWIDGET",
            "android.permission.BIND_DEVICE_ADMIN",
            "android.permission.BIND_INPUT_METHOD",
            "android.permission.BIND_REMOTEVIEWS",
            "android.permission.BIND_WALLPAPER",
            "android.permission.BLUETOOTH",
            "android.permission.BLUETOOTH_ADMIN",
            "android.permission.BRICK",
            "android.permission.BROADCAST_PACKAGE_REMOVED",
            "android.permission.BROADCAST_SMS",
            "android.permission.BROADCAST_STICKY",
            "android.permission.BROADCAST_WAP_PUSH",
            "android.permission.CALL_PHONE",
            "android.permission.CALL_PRIVILEGED",
            "android.permission.CAMERA",
            "android.permission.CHANGE_COMPONENT_ENABLED_STATE",
            "android.permission.CHANGE_CONFIGURATION",
            "android.permission.CHANGE_NETWORK_STATE",
            "android.permission.CHANGE_WIFI_MULTICAST_STATE",
            "android.permission.CHANGE_WIFI_STATE",
            "android.permission.CLEAR_APP_CACHE",
            "android.permission.CLEAR_APP_USER_DATA",
            "android.permission.CONTROL_LOCATION_UPDATES",
            "android.permission.DELETE_CACHE_FILES",
            "android.permission.DELETE_PACKAGES",
            "android.permission.DEVICE_POWER",
            "android.permission.DIAGNOSTIC",
            "android.permission.DISABLE_KEYGUARD",
            "android.permission.DUMP",
            "android.permission.EXPAND_STATUS_BAR",
            "android.permission.FACTORY_TEST",
            "android.permission.FLASHLIGHT",
            "android.permission.FORCE_BACK",
            "android.permission.GET_ACCOUNTS",
            "android.permission.GET_PACKAGE_SIZE",
            "android.permission.GET_TASKS",
            "android.permission.GLOBAL_SEARCH",
            "android.permission.HARDWARE_TEST",
            "android.permission.INJECT_EVENTS",
            "android.permission.INSTALL_LOCATION_PROVIDER",
            "android.permission.INSTALL_PACKAGES",
            "android.permission.INTERNAL_SYSTEM_WINDOW",
            "android.permission.KILL_BACKGROUND_PROCESSES",
            "android.permission.MANAGE_ACCOUNTS",
            "android.permission.MANAGE_APP_TOKENS",
            "android.permission.MASTER_CLEAR",
            "android.permission.MODIFY_AUDIO_SETTINGS",
            "android.permission.MODIFY_PHONE_STATE",
            "android.permission.MOUNT_FORMAT_FILESYSTEMS",
            "android.permission.MOUNT_UNMOUNT_FILESYSTEMS",
            "android.permission.NFC",
            "android.permission.PERSISTENT_ACTIVITY",
            "android.permission.PROCESS_OUTGOING_CALLS",
            "android.permission.READ_CALENDAR",
            "android.permission.READ_CONTACTS",
            "android.permission.READ_FRAME_BUFFER",
            "android.permission.READ_INPUT_STATE",
            "android.permission.READ_LOGS",
            "android.permission.READ_PHONE_STATE",
            "android.permission.READ_SMS",
            "android.permission.RECEIVE_WAP_PUSH",
            "android.permission.REORDER_TASKS",
            "android.permission.RESTART_PACKAGES",
            "android.permission.SEND_SMS",
            "android.permission.SET_ACTIVITY_WATCHER",
            "android.permission.SET_ALWAYS_FINISH",
            "android.permission.SET_ANIMATION_SCALE",
            "android.permission.SET_DEBUG_APP",
            "android.permission.SET_ORIENTATION",
            "android.permission.SET_PREFERRED_APPLICATIONS",
            "android.permission.SET_PROCESS_LIMIT",
            "android.permission.SET_TIME",
            "android.permission.SET_TIME_ZONE",
            "android.permission.SET_WALLPAPER",
            "android.permission.SET_WALLPAPER_HINTS",
            "android.permission.SIGNAL_PERSISTENT_PROCESSES",
            "android.permission.STATUS_BAR",
            "android.permission.SUBSCRIBED_FEEDS_READ",
            "android.permission.SUBSCRIBED_FEEDS_WRITE",
            "android.permission.UPDATE_DEVICE_STATS",
            "android.permission.USE_CREDENTIALS",
            "android.permission.USE_SIP",
            "android.permission.VIBRATE",
            "android.permission.WAKE_LOCK",
            "android.permission.WRITE_APN_SETTINGS",
            "android.permission.WRITE_CALENDAR",
            "android.permission.WRITE_CONTACTS",
            "android.permission.WRITE_GSERVICES",
            "android.permission.WRITE_SMS",
            "android.permission.WRITE_SYNC_SETTINGS",
            "com.android.browser.permission.READ_HISTORY_BOOKMARKS",
            "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS",
            "android.permission.REQUEST_INSTALL_PACKAGES",
            "moe.shizuku.manager.permission.API_V23",
            "android.permission.QUERY_ALL_PACKAGES",
            "android.permission.REQUEST_DELETE_PACKAGES"
        ],
        "requestListOnStartup": [
            "android.permission.READ_PHONE_STATE",
            "android.permission.WRITE_EXTERNAL_STORAGE"
        ]
    },
    "plugins": {
        "org.autojs.autojspro.plugin.mlkit.ocr": "1.1",
        "org.autojs.autojspro.ocr.v2":"1.3",
        "com.tomato.ocr":"1.0",
        "com.hraps.ocr32":"1.0",
        "com.hraps.ocr":"2.0.0",
        "cn.lzx284.p7zip":"1.2.1",
        "com.hraps.pytorch":"1.0",
        "org.autojs.plugin.ffmpeg":"1.1"
    },
    "scripts": {},
    "signingConfig": {
        "alias": "zjh336",
        "keystore": "",
        "uuid": ""
    },
    "useFeatures": [],
    "versionCode": 1000,
    "versionName": "1.0.0"
};

export default {
    template: template,
    name: 'FileManage',
    inject: ['validSelectDevice', 'sendMsgToClient', 'remoteExecuteScript', 'getMonacoEditorComplete', 'updateFileDialogIsMin'],
    props: {
        deviceInfo: { // 设备信息
            type: Object,
            default() {
                return {
                    startPreview: false,
                    deviceUuid: '',
                    standardWidth: null,
                    standardHeight: null,
                    standardConvert: false,
                    offsetX: 0,
                    offsetY: 0
                }
            }
        },
        screenDirection: {
            type: String,
            default: '横屏'
        }
    },
    data() {
        return {
            isInit:false,
            fileEditVisible:false, // 文件编辑器可见
            phoneFileEditVisible:false,
            phoneImagePreviewVisible:false,// 手机端图片预览
            phoneImageBase64:'',// 图片内容
            fileEditorName:'',// 文件编辑器名称
            phoneFileEditorName:'',
            fileSavePath: '',// 文件保存路径
            phoneFileSavePath:'',// 手机端文件保存路径
            phoneFileCacheArr:[],// 手机端文件缓存列表
            phoneFileSelectIndex:-1, // 手机端文件选择下标
            /**
             * fileSavePath:文件保存路径
             * fileName: 文件名
             * fileContent: 文件内容
             */
            scriptEditor:null,
            phoneScriptEditor:null,
            fileActiveName:'web',
            webSyncPath: "/",
            phoneSyncPath: "/",
            autoSyncWebSyncPath:true,
            autoSyncPhoneSyncPath:true,
            phoneFileTableRandomKey:Math.random(),
            breadcrumbList: [{label: '根目录', value: 'undefined'}], // 面包屑
            phoneBreadcrumbList: [{label: '根目录', value: '/sdcard/'}],
            fileLoading: false,// 加载文件loading
            phoneFileLoading: false,// 手机端加载文件loading
            checkAllFile: false,// 全选文件
            phoneCheckAllFile: false,// 手机端全选文件
            previewList: [],// 单个上传
            uploadFileList: [],// 需要上传的文件列表
            accept:'.jpg,.jpeg,.png,.pdf,.JPG,.JPEG,.PNG,.PDF,.doc,.docx,.xlsx,.xls,.ppt,.pptx,.rar,.zip,.txt,.mp4,.flv,.rmvb,.avi,.wmv,.js',
            curFileData: {},// 选中的文件数据
            copyFileList: [],// 复制文件列表
            phoneCopyFileList:[],// 手机端复制文件列表
            moveFileList: [],// 移动文件列表
            phoneMoveFileList: [],// 手机端移动文件列表
            absolutePrePath: '',// 绝对路径前缀
            fileList: [], // 文件列表
            phoneFileList:[], // 手机文件列表
            packageProjectActive:0,
            packageProjectDialog: false,
            projectJsonObj:{ // 缓存project对象

            },
            packageProjectStepLoading: false,
            keyStoreArr:[],// 自定义签名数组
            packageProject:{
                appName:'',//
                packageName:'',//
                versionName:'',
                versionCode:'',
                appIcon:'',
                openNodeJs:false,
                autoOpen:false,
                plugins:[],
                abis:['armeabi-v7a','arm64-v8a'],
                hideLogs:true,
                splashText:'',
                splashIcon:'',
                customSignStorePath:''
            },
            packageProjectRules:{
                appName: [{ required: true, message: '请填写应用名称', trigger: 'blur' }],
                packageName: [{ required: true, message: '请填写应用包名', trigger: 'blur' }],
                versionName: [{ required: true, message: '请填写版本名称', trigger: 'blur' }],
                versionCode: [{ required: true, message: '请填写版本号', trigger: 'blur' }],
                abis:[{ required: true, message: '请选择CPU架构', trigger: 'change' }],
                appIcon:[{ required: true, trigger: 'change' , validator: (rules, value, cb) => {
                        if(!value){
                            return cb()
                        }
                        if (value.endsWith("png") || value.endsWith("jpg") || value.endsWith("jpeg")) {
                            return cb()
                        }
                        return cb(new Error('只能使用png、jpg、jpeg格式图片!'))
                    }}],
                splashIcon:[{ required: true, trigger: 'change' , validator: (rules, value, cb) => {
                        if(!value){
                            return cb()
                        }
                        if (value.endsWith("png") || value.endsWith("jpg") || value.endsWith("jpeg")) {
                            return cb()
                        }
                        return cb(new Error('只能使用png、jpg、jpeg格式图片!'))
                    }}],
                customSignStorePath: [{ required: true, trigger: 'change' , validator: (rules, value, cb) => {
                        if(!this.keyStoreArr || !this.keyStoreArr.length){
                            return cb(new Error('请先在公共文件模块，生成自定义签名!'))
                        }
                        let arr = this.keyStoreArr.filter(item=>item === value);
                        if (!arr || !arr.length) {
                            return cb(new Error('请选择自定义签名!'))
                        }
                        return cb()
                    }}]
            },
            alreadyInitPackageTemplate:false,// 是否已经初始化完成打包模板
            alreadyUploadProjectRes:false,// 是否已经上传项目资源
            alreadyHandlerPackageRes:false,// 是否已处理打包资源
            alreadyCompletePackageProject:false,// 是否已完成打包项目
            packageProjectMessage:'',// 打包日志信息
        }
    },
    mounted() {
        let _that = this;
        $.ajax({
            url: getContext() + "/attachmentInfo/getAbsolutePrePath",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data) {
                    if (data.isSuccess) {
                        _that.absolutePrePath = data.data;
                    }
                }
            },
            error: function (msg) {
            }
        });
    },
    computed: {
        uploadPrePathName() {
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            return toPath.replace(/\//g, "\\");
        },
        navigatePath(){
            let toPath = this.breadcrumbList.map(item=>item.label).join('/');
            return toPath
        },
        checkFileCount() { // 已选文件数量
            return this.fileList.filter(item => item.check).length;
        },
        phoneCheckFileCount(){ // 手机端已选文件数量
            return this.phoneFileList.filter(item => item.check).length;
        },
        allowMoveFileList() { // 允许移动的文件列表
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            let replacePath = toPath.replace(/\//g, "\\");
            // 当前完整目录
            let curToPath = this.absolutePrePath + replacePath;
            // 当前目录是已选文件子目录的 需要过滤掉  当前目录是已选文件所在的目录是需要过滤掉
            return this.moveFileList.filter(item => {
                return !curToPath.startsWith(item.pathName) && curToPath !== item.parentPathName
            });
        },
        phoneAllowMoveFileList() { // 手机端允许移动的文件列表
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            // 当前目录是已选文件子目录的 需要过滤掉  当前目录是已选文件所在的目录是需要过滤掉
            return this.phoneMoveFileList.filter(item => {
                return !toPath.startsWith(item.pathName) && toPath !== item.parentPathName
            });
        },
        copyFileNames() {
            return this.copyFileList.map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        phoneCopyFileNames() {
            return this.phoneCopyFileList.map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        allowMoveFileNames() {
            return this.allowMoveFileList.map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        phoneAllowMoveFileNames() {
            return this.phoneAllowMoveFileList.map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        checkFileNames() {
            return this.fileList.filter(item => item.check).map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        phoneCheckFileNames() {
            return this.phoneFileList.filter(item => item.check).map(item => (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)).join(',');
        },
        // 上传进度数组
        uploadPercentageArr(){
            return this.uploadFileList.map(item=>item.percentage);
        },
        // 手机端编辑器缓存数组文件是否修改记录数组
        phoneFileChangeArr(){
           return this.phoneFileCacheArr.map(item=> item.sourceFileContent !== item.fileContent);
        }
    },
    watch:{
        uploadPercentageArr(arr){
            if(arr && arr.length>0){
                let completeArr = arr.filter(item=>Number(item)===Number(100));
                // 全部上传完成
                if(arr.length === completeArr.length){
                    this.uploadFileList = [];// 清空上传列表
                    let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;

                    setTimeout(()=>{
                        // 重新加载文件列表
                        this.queryFileList(toPath);
                    },200);
                }
            }
        },
        packageProjectActive(val){
            // 第二步  初始化打包模板
            if(val === 1){
                // 检查是否已经初始化完成打包模板
                this.alreadyInitPackageTemplate = this.checkWebPackageTemplate();
            // 第三步  上传项目资源
            } else if(val === 2){
                // 检测项目资源是否已上传完成
                this.alreadyUploadProjectRes = this.checkProjectRes()
            // 第四步  打包资源处理
            } else if(val === 3){
                // 检测打包资源是否已处理完成
                this.alreadyHandlerPackageRes = this.checkPackageResHandlerStatus();
            // 第五步  打包项目
            } else if(val === 4){
                // 检测是否已完成打包
                this.alreadyCompletePackageProject = this.checkPackageProject();
            }
        }
    },
    methods: {
        // 初始化方法
        init() {
            if(!this.isInit){
                let relativeFilePath = this.deviceInfo.deviceUuid;
                if(relativeFilePath){
                    // 加载文件列表
                    this.queryFileList(relativeFilePath);
                    this.breadcrumbList = [{label: '根目录', value: this.deviceInfo.deviceUuid}]
                }
                // 获取手机端文件目录
                this.updatePhoneDirCache(this.phoneBreadcrumbList.map(item => item.value).join('/'));
            }
            this.isInit = true;
        },
        // 取消上传
        cancelUpload(i) {
            this.uploadFileList.splice(i, 1);
            window.newxhr.abort();
        },
        // 文件上传按钮点击
        uploadFileClick(){
            this.$refs.input.value = null;
            this.$refs.input.click();
        },
        // 文件修改事件触发
        handleChange(ev) {
            const files = ev.target.files;
            if (!files) return;
            this.uploadFiles(files);
        },
        // 上传文件
        uploadFiles(files) {
            const postFiles = Array.prototype.slice.call(files);
            if (postFiles.length === 0) { return }
            let tempIndex = 0;
            postFiles.forEach(rawFile => {
                // 检查文件是否合法
                if (this.beforeUpload(rawFile)) {
                    this.handleStart(rawFile, tempIndex++);
                    // 提交上传
                    this.submitUpload(rawFile)
                }
            })
        },
        handleStart(rawFile, tempIndex) {
            rawFile.uid = Date.now() + tempIndex;
            const file = {
                status: 'ready',
                fileName: rawFile.name,
                size: rawFile.size,
                percentage: 0,
                uid: rawFile.uid,
                raw: rawFile,
                fileType: rawFile.name.substring(rawFile.name.lastIndexOf('.') + 1),
                uploadFlag: true,
                check: false,
                isCancel: false
            };
            this.uploadFileList.push(file);
        },
        // 检查文件是否合法
        beforeUpload(file) { // 上传之前的钩子函数，验证大小，验证支持上传的文件
            if (file.size > 500 * 1024 * 1024) {
                this.$message({
                    message: `${file.name}暂不支持上传大小超过500M的文件!`,
                    type: 'warning'
                });
                return false
            }
            const testmsg = file.name.substring(file.name.lastIndexOf('.'));
            const test = this.accept.split(',').includes(testmsg);
            if (!test) {
                this.$message({
                    /* 上传文件只能是${this.accept}格式!*/
                    message: `${file.name}文件不支持上传噢`,
                    type: 'warning'
                });
                return false
            }
            return test
        },
        uploadProgress(percent, uid) { // 上传进度条
            this.uploadFileList.forEach(file => {
                if (file.uid === uid) {
                    file.percentage = percent
                }
            })
        },
        // 提交上传
        submitUpload(item) {
            const uid = item.uid;
            const param = new FormData();
            this.uploadFileList.forEach(file => {
                if (file.uid === uid) {
                    file.status = 'uploading';
                    param.append('file', file.raw);
                    param.append('pathName', this.uploadPrePathName)
                }
            });
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/uploadFileSingle",
                type: 'post',
                data: param,
                processData: false,
                contentType: false,
                dataType: "json",
                xhr: function() {
                    let newxhr = new XMLHttpRequest();
                    // 添加文件上传的监听
                    // onprogress:进度监听事件，只要上传文件的进度发生了变化，就会自动的触发这个事件
                    newxhr.upload.onprogress = function(e) {
                        const percent = (e.loaded / e.total) * 100 | 0;
                        _that.uploadProgress(percent, uid)
                    };
                    window.newxhr = newxhr;
                    return newxhr
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                        }
                    }
                },
                error: function (msg) {
                }
            });
        },
        // 查询文件列表
        queryFileList(relativeFilePath) {
            this.fileLoading = true;
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                // contentType: "application/json",
                data: {
                    "relativeFilePath": relativeFilePath
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            let pathNameArr = _that.copyFileList.map(item => item.pathName);
                            let pathNameArr2 = _that.moveFileList.map(item => item.pathName);
                            data.data.forEach(item => {
                                item.check = pathNameArr.includes(item.pathName) || pathNameArr2.includes(item.pathName) || false
                            });
                            _that.fileList = data.data;
                        }
                    }
                    setTimeout(() => {
                        _that.fileLoading = false
                    }, 200)
                },
                error: function (msg) {
                    setTimeout(() => {
                        _that.fileLoading = false
                    }, 200)
                }
            });
        },
        // 监听拖拽
        onDrop(e) {
            this.uploadFiles(e.dataTransfer.files)
        },
        onDragover(){

        },
        // 操作
        operateFun(code) {
            switch (code) {
                case 'copy': {
                    if (this.moveFileList && this.moveFileList.length > 0) {
                        this.moveFileList = [];
                    }
                    // 设置复制文件集合
                    this.copyFileList = this.fileList.filter(item => item.check);
                    break;
                }
                case 'paste': {
                    let fileNames = this.copyFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');
                    let toName = this.breadcrumbList[this.breadcrumbList.length - 1].label;
                    let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                    let replacePath = toPath.replace(/\//g, "\\");
                    window.ZXW_VUE.$confirm('是否确认将' + fileNames + '复制到' + toName + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let sourcePathList = this.copyFileList.map(item => item.pathName);
                        let _that = this;
                        $.ajax({
                            url: getContext() + "/attachmentInfo/copyFileBatch",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "sourcePathList": sourcePathList,
                                "targetFolderPath": _that.absolutePrePath + replacePath
                            }),
                            success: function (data) {
                                if (data) {
                                    if (data.isSuccess) {
                                        window.ZXW_VUE.$notify.success({message: '复制成功', duration: '1000'});
                                        // 清空文件列表
                                        _that.copyFileList = [];
                                        // 重新加载文件列表
                                        _that.queryFileList(toPath);
                                    }
                                }
                            },
                            error: function (msg) {
                            }
                        });
                    });
                    break;
                }
                case 'cancel': {
                    this.copyFileList = [];
                    this.moveFileList = [];
                    break;
                }
                case 'move': {
                    if (this.copyFileList && this.copyFileList.length > 0) {
                        this.copyFileList = [];
                    }
                    // 设置移动文件集合
                    this.moveFileList = this.fileList.filter(item => item.check);
                    break;
                }
                case 'moveTo': {
                    let toName = this.breadcrumbList[this.breadcrumbList.length - 1].label;
                    let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                    // 当前完整目录
                    let curToPath = this.absolutePrePath + toPath;

                    // 当前目录是已选文件子目录的
                    let fileNames = this.allowMoveFileList.map(item => {
                        return (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');

                    window.ZXW_VUE.$confirm('是否确认将' + fileNames + '移动到' + toName + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let sourcePathList = this.allowMoveFileList.map(item => item.pathName);
                        let _that = this;
                        $.ajax({
                            url: getContext() + "/attachmentInfo/moveFileBatch",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify({
                                "sourcePathList": sourcePathList,
                                "targetFolderPath": curToPath
                            }),
                            success: function (data) {
                                if (data) {
                                    if (data.isSuccess) {
                                        window.ZXW_VUE.$notify.success({message: '移动成功', duration: '1000'});
                                        // 清空文件列表
                                        _that.moveFileList = [];
                                        // 重新加载文件列表
                                        _that.queryFileList(toPath);
                                    }
                                }
                            },
                            error: function (msg) {
                            }
                        });
                    });
                    break;
                }
                case 'remove': {
                    // 当前目录是已选文件子目录的
                    let checkFileList = this.fileList.filter(item => item.check);
                    let fileNames = checkFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');
                    let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                    window.ZXW_VUE.$confirm('是否确认删除' + fileNames + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let filePathList = checkFileList.map(item => item.pathName);
                        let _that = this;
                        $.ajax({
                            url: getContext() + "/attachmentInfo/deleteFileBatch",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify(filePathList),
                            success: function (data) {
                                if (data) {
                                    if (data.isSuccess) {
                                        window.ZXW_VUE.$notify.success({message: '删除成功', duration: '1000'});
                                        // 重新加载文件列表
                                        _that.queryFileList(toPath);
                                    }
                                }
                            },
                            error: function (msg) {
                            }
                        });
                    });
                    break;
                }
                case 'syncToPhone':{
                    let checkFileList = this.fileList.filter(item => item.check);
                    let fileNames = checkFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    });
                    window.ZXW_VUE.$prompt('是否确认同步'+fileNames.length+'个文件到手机端,文件夹内部不会递归同步,请输入手机端路径(以/sdcard为根目录的相对路径)!', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        inputValue: this.phoneSyncPath,
                        inputValidator: function(val) {
                            if(val){
                                if(val.startsWith("/sdcard")){
                                    return "无需以/sdcard开头"
                                }
                                if(!val.startsWith("/")){
                                    return "必须以/开头"
                                }
                                if(!val.endsWith("/")){
                                    return "必须以/结尾"
                                }
                            } else {
                                return "不能为空";
                            }
                            return true;
                        }
                    }).then(({value}) => {
                        // 设置同步文件集合  暂不支持同步目录
                        let webSyncToPhoneArr = this.fileList.filter(item => item.check);
                        this.webSyncToPhoneFun(webSyncToPhoneArr,value);
                    }).catch(() => {
                    });
                    break;
                }
            }
        },
        // 手机端文件操作
        phoneOperateFun(code){
            switch (code) {
                case 'copy': {
                    if (this.phoneMoveFileList && this.phoneMoveFileList.length > 0) {
                        this.phoneMoveFileList = [];
                    }
                    // 设置复制文件集合
                    this.phoneCopyFileList = this.phoneFileList.filter(item => item.check);
                    break;
                }
                case 'paste': {
                    // TODO 手机端复制不支持文件夹
                    let fileNames = this.phoneCopyFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');
                    let toName = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].label;
                    let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
                    window.ZXW_VUE.$confirm('是否确认将' + fileNames + '复制到' + toName + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let sourcePathList = this.phoneCopyFileList.map(item => item);
                        let remoteScript = '';
                        sourcePathList.forEach(item=>{
                            remoteScript += `files.copy('${item.pathName}', '${toPath}'+'${(item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType)}');`;
                        });
                        this.remoteExecuteScript(remoteScript);
                        setTimeout(()=>{
                            // 清空文件列表
                            this.phoneCopyFileList = [];
                            // 刷新手机目录
                            this.refreshPhoneDir();
                        },500);
                    });
                    break;
                }
                case 'cancel': {
                    this.phoneCopyFileList = [];
                    this.phoneMoveFileList = [];
                    break;
                }
                case 'move': {
                    if (this.phoneCopyFileList && this.phoneCopyFileList.length > 0) {
                        this.phoneCopyFileList = [];
                    }
                    // 设置移动文件集合
                    this.phoneMoveFileList = this.phoneFileList.filter(item => item.check);
                    break;
                }
                case 'moveTo': {
                    let toName = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].label;
                    let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;

                    // 当前目录是已选文件子目录的
                    let fileNames = this.phoneAllowMoveFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');

                    window.ZXW_VUE.$confirm('是否确认将' + fileNames + '移动到' + toName + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let sourcePathList = this.phoneAllowMoveFileList;
                        let remoteScript = '';
                        sourcePathList.forEach(item=>{
                            remoteScript += `files.move('${item.pathName}', '${toPath}'+'${(item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType)}');`;
                        });
                        this.remoteExecuteScript(remoteScript);
                        setTimeout(()=>{
                            this.phoneMoveFileList = [];
                            // 刷新手机目录
                            this.refreshPhoneDir();
                        },500);
                    });
                    break;
                }
                case 'remove': {
                    // 当前目录是已选文件子目录的
                    let checkFileList = this.phoneFileList.filter(item => item.check);
                    let fileNames = checkFileList.map(item => {
                        return (item.isDirectory || !item.fileType)? item.fileName : (item.fileName + "." + item.fileType);
                    }).join(',');
                    window.ZXW_VUE.$confirm('是否确认删除' + fileNames + '?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'info'
                    }).then(() => {
                        let filePathList = checkFileList.map(item => item.pathName);
                        let remoteScript = '';
                        filePathList.forEach(item=>{
                            remoteScript += `files.removeDir('${item}');`;
                        });
                        this.remoteExecuteScript(remoteScript);
                        setTimeout(()=>{
                            // 刷新手机目录
                            this.refreshPhoneDir();
                        },500);
                    });
                    break;
                }
                case 'syncToWeb':{
                    let checkFileList = this.phoneFileList.filter(item => item.check);
                    let fileNames = checkFileList.map(item => {
                        return (item.isDirectory || !item.fileType) ? item.fileName : (item.fileName + "." + item.fileType);
                    });
                    window.ZXW_VUE.$prompt('是否确认同步'+fileNames.length+'个文件到web端,文件夹内部不会递归同步,请输入web端路径', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        inputValue: this.webSyncPath,
                        inputValidator: function(val) {
                            if(val){
                                if(!val.startsWith("/")){
                                    return "必须以/开头"
                                }
                                if(!val.endsWith("/")){
                                    return "必须以/结尾"
                                }
                            } else {
                                return "不能为空";
                            }
                            return true;
                        }
                    }).then(({value}) => {
                        // 设置同步文件集合  暂不支持同步目录
                        let remoteScript = `let webPath = utilsObj.getDeviceUUID()+ '${value}';\r\n`;
                        checkFileList.forEach(item=>{
                            if(item.isDirectory){
                                remoteScript +=`utilsObj.request("/attachmentInfo/createFolder?folderName="+webPath+'${item.fileName}',"GET",null,()=>{toastLog("同步完成")});\r\n`;
                            } else {
                                remoteScript +=`utilsObj.uploadFileToServer('${item.pathName}',webPath + '${item.fileName}' + '.'+ '${item.fileType}',()=>{toastLog("同步完成")});\r\n`;
                            }
                        });
                        this.remoteExecuteScript(remoteScript);
                        setTimeout(()=>{
                            // 刷新web目录
                            this.refreshWebDir();
                        },500);
                    }).catch(() => {
                    });
                    break;
                }
            }
        },
        syncToWebSingle(row){
            window.ZXW_VUE.$prompt('是否确认同步'+row.pathName+'到web端,文件夹内部不会递归同步,请输入web端路径', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: this.webSyncPath,
                inputValidator: function(val) {
                    if(val){
                        if(!val.startsWith("/")){
                            return "必须以/开头"
                        }
                        if(!val.endsWith("/")){
                            return "必须以/结尾"
                        }
                    } else {
                        return "不能为空";
                    }
                    return true;
                }
            }).then(({value}) => {
                this.phoneFileLoading = true;
                let relativeFilePath = this.deviceInfo.deviceUuid  + value + ((row.isDirectory || !row.fileType) ? row.fileName : row.fileName +'.' + row.fileType);
                // 文件内容变化后处理函数
                handlerByFileChange(relativeFilePath,()=>{
                    // 设置同步文件集合  暂不支持同步目录
                    let remoteScript = ``;
                    if(row.isDirectory){
                        remoteScript +=`utilsObj.request("/attachmentInfo/createFolder?folderName="${relativeFilePath},"GET",null,()=>{toastLog("同步完成")});\r\n`;
                    } else {
                        remoteScript +=`utilsObj.uploadFileToServer('${row.pathName}','${relativeFilePath}',()=>{toastLog("同步完成")});\r\n`;
                    }
                    this.remoteExecuteScript(remoteScript);
                },()=>{
                    this.phoneFileLoading = false;
                    // 刷新web目录
                    this.refreshWebDir();
                });
            }).catch(() => {
            });
        },
        // web端同步到手机公共方法
        webSyncToPhoneFun(webSyncToPhoneArr,value){
            let remoteExecuteScriptContent = "";
            webSyncToPhoneArr.forEach(row=>{
                let downloadFilUrl = getContext() +"/"+ row.previewUrl;
                // 如果是目录 则只需要远程创建目录
                if(row.isDirectory){
                    // 创建目录代码 如果不是/ 则需要创建目录
                    let createWithDirsCode = value !=='/' ? "files.createWithDirs('/sdcard"+value + row.fileName+"/');" : "";
                    // 拼接代码
                    remoteExecuteScriptContent += createWithDirsCode;
                } else {
                    // 如果有值且  是以/开头
                    let localFileUrl = value  + ((row.isDirectory || !row.fileType) ? row.fileName : (row.fileName + "." + row.fileType));
                    // 创建目录代码 如果不是/ 则需要创建目录
                    let createWithDirsCode = value !=='/' ? "files.createWithDirs('/sdcard"+value+"');" : "";
                    let script = createWithDirsCode + "utilsObj.downLoadFile('"+downloadFilUrl+"','"+localFileUrl+"',()=>{});";
                    // 拼接代码
                    remoteExecuteScriptContent += script;
                }
            });
            this.remoteExecuteScript(remoteExecuteScriptContent);
        },
        // 全选
        checkAllFileChange() {
            this.fileList.forEach(item => {
                this.$set(item, 'check', this.checkAllFile);
            });
        },
        // 手机端全选
        phoneCheckAllFileChange(){
            this.phoneFileList.forEach(item => {
                this.$set(item, 'check', this.phoneCheckAllFile);
            });
        },
        // 文件名点击
        fileClick(row) {
            // this.$set(row,'check',!row.check);
        },
        // 文件名双击
        fileNameDbClick(row) {
            // 如果是目录
            if (row.isDirectory) {
                // 记录到面包屑导航栏
                let pathName = row.pathName;
                let index = pathName.indexOf(this.deviceInfo.deviceUuid);
                pathName = pathName.substring(index, pathName.length);
                let array = pathName.indexOf("\\") !==-1 ? pathName.split("\\") : pathName.split("/");

                // 面包屑数组
                let breadcrumbArr = [];
                let pathArr = [];
                for (let i = 0; i < array.length; i++) {
                    pathArr.push(array[i]);
                    breadcrumbArr.push({
                        label: i === 0 ? "根目录" : array[i],
                        value: pathArr.join("/")
                    });
                }
                this.breadcrumbList = breadcrumbArr;
                // 默认加载最后一个
                this.breadcrumbChange(this.breadcrumbList[this.breadcrumbList.length - 1], (this.breadcrumbList.length - 1))
                // 文件 空白窗口打开
            } else {
                if(['png','jpg','jpeg'].includes(row.fileType)){
                    window.open(getContext() + "/" + row.previewUrl)
                } else if(['zip','rar','apk'].includes(row.fileType)){
                    window.ZXW_VUE.$message.warning('暂不支持编辑此类文件');
                    return false;
                } else {
                    this.fileEditVisible = true;
                    window.removeEventListener('keydown',this.editorDialogSaveListener);
                    window.addEventListener('keydown',this.editorDialogSaveListener,false);
                    this.fileEditorName = row.fileName + '.' + row.fileType;
                    this.fileSavePath = row.previewUrl.replace('uploadPath/autoJsTools','').replace(this.fileEditorName,'');
                    let _that = this;
                    $.ajax({
                        url: getContext() + "/" +row.previewUrl +"?t="+(new Date().getTime()),
                        type: 'get',
                        async: false,
                        dataType:"TEXT", //返回值的类型
                        success: function (res) {
                            // 初始化文件编辑器
                            initFileEditor(_that,'scriptEditor','fileEditor',_that.getMonacoEditorComplete,res,'javascript','vs-dark',(e,value)=>{
                            })
                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    });
                }
            }
        },
        // 手机端文件双击
        phoneFileNameDbClick(row){
            // 如果是目录
            if (row.isDirectory) {
                // 记录到面包屑导航栏
                let pathName = row.pathName;
                pathName = row.pathName.replace('/','');
                let array = pathName.split("/");
                  // 面包屑数组
                 let breadcrumbArr = [];
                 let pathArr = [];
                 for (let i = 0; i < array.length; i++) {
                     pathArr.push(array[i]);
                     breadcrumbArr.push({
                         label: i === 0 ? "根目录" : array[i],
                         value: '/'+pathArr.join("/")+'/'
                     });
                 }
                this.phoneBreadcrumbList = breadcrumbArr;
                 // 默认加载最后一个
                this.refreshPhoneDir();
                // 文件 空白窗口打开
            } else {
                if(['png','jpg','jpeg'].includes(row.fileType)){
                    this.phoneFileLoading = true;
                    this.updatePhoneFileCache(row);
                } else if(['zip','rar','apk'].includes(row.fileType)){
                    window.ZXW_VUE.$message.warning('暂不支持编辑此类文件');
                    return false;
                } else {
                    this.phoneFileLoading = true;
                    window.removeEventListener('keydown',this.phoneEditorDialogSaveListener);
                    window.addEventListener('keydown',this.phoneEditorDialogSaveListener,false);
                    this.updatePhoneFileCache(row);
                }
            }
        },
        // web端文件编辑器弹窗保存监听
        editorDialogSaveListener(e){
            if(e.ctrlKey && e.keyCode === 83 && this.fileEditVisible){
                e.stopPropagation();
                e.preventDefault();
                if(!this.fileEditorName){
                    return;
                }
                let scriptFile = new File([this.scriptEditor.getValue()], this.fileEditorName, {
                    type: "text/plain",
                });
                const param = new FormData();
                param.append('file', scriptFile);
                param.append('pathName', this.fileSavePath);
                let _that = this;
                $.ajax({
                    url: getContext() + "/attachmentInfo/uploadFileSingle",
                    type: 'post',
                    data: param,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '保存成功', duration: '1000'});
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
                return false;
            }
        },
        // 手机端文件编辑器弹窗保存监听
        phoneEditorDialogSaveListener(e){
            if(!this.phoneFileEditVisible){
                return;
            }
            // 保存 ctrl+s
            if(e.ctrlKey && e.keyCode === 83){
                // 获取当前点击的文件对象
                let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
                if(!fileObj){
                    return;
                }
                let remoteScript = `let writableTextFile = files.write('${fileObj.fileSavePath}',decodeURI($base64.decode('${btoa(encodeURI(fileObj.fileContent))}')));`;
                this.remoteExecuteScript(remoteScript);
                // 更新原始缓存值
                fileObj.sourceFileContent = fileObj.fileContent;
                e.stopPropagation();
                e.preventDefault();
                return false;
            // 最小化 ctrl+3
            }else if(e.ctrlKey && (e.keyCode === 51 || e.keyCode === 99)){
                e.stopPropagation();
                e.preventDefault();
                this.phoneMinFileEditorDialog();
            // 运行当前 ctrl+1
            }else if(e.ctrlKey && (e.keyCode === 49 || e.keyCode === 97)){
                e.stopPropagation();
                e.preventDefault();
                this.phoneRunScriptByDialog();
            // 停止全部 ctrl+2
            }else if(e.ctrlKey && (e.keyCode === 50 || e.keyCode === 98)){
                e.stopPropagation();
                e.preventDefault();
                this.phoneStopAllScript();
            // 切换tab ctrl + 0
            }else if(e.ctrlKey && (e.keyCode === 48 || e.keyCode === 96)){
                e.stopPropagation();
                e.preventDefault();
                let length = this.phoneFileCacheArr.length;
                if(this.phoneFileSelectIndex ===  length - 1){
                    this.phoneFileSelectIndex = 0
                } else {
                    this.phoneFileSelectIndex = this.phoneFileSelectIndex+1;
                }
                this.phoneFileEditorArrClick(this.phoneFileSelectIndex);
            }
        },
        // 打包项目
        phonePackageProject(){
            let projectFileArr = this.phoneFileList.filter(item=>item.fileType==='json' && item.fileName === 'project');
            if(!projectFileArr || projectFileArr.length===0){
                window.ZXW_VUE.$confirm('未检测到project.json,是否需要在当前目录生成文件?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'info'
                }).then(() => {
                    // 将json写入本地文件
                    let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
                    let jsonObj = JSON.parse(JSON.stringify(defaultProjectJSON));
                    // 默认项目名称
                    jsonObj.name = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].label || "";
                    let targetPath = toPath + "/project.json";
                    let remoteScript = `let writableTextFile = files.write('${targetPath}',decodeURI($base64.decode('${btoa(encodeURI(JSON.stringify(jsonObj,"","\t")))}')));`;
                    this.remoteExecuteScript(remoteScript);
                    // 刷新手机目录
                    this.refreshPhoneDir();
                });
                return;
            }
            // 读取当前目录project.json文件
            this.getPhoneProjectJson((projectJson)=>{
                if(!projectJson){
                    window.ZXW_VUE.$message.warning('未检测到project.json');
                } else {
                    this.projectJsonObj = JSON.parse(projectJson);
                    // 打开弹窗
                    this.packageProjectDialog = true;
                    // 关闭loading
                    this.packageProjectStepLoading = false;
                    // 重置步骤
                    this.packageProjectActive = 0;
                    // 初始化配置
                    this.initProjectJsonFun();
                    // 加载自定义签名数组
                    this.loadCustomSignDict();
                }
            })
        },
        // 下一步
        nextSteps(){
            // 第一步
            if(this.packageProjectActive === 0){
                // 校验配置表单
                this.$refs["packageProjectFirst"].validate((valid) => {
                    if (valid) {
                        // 校验通过调用保存配置方法
                        this.saveProjectJsonFun();
                        this.packageProjectActive++;
                    } else {
                        window.ZXW_VUE.$message.warning('请将信息补充完整！');
                    }
                });
            // 第二步
            } else if(this.packageProjectActive === 1){
                if(this.alreadyInitPackageTemplate){
                    this.packageProjectActive++;
                } else {
                    window.ZXW_VUE.$message.warning('请先初始化打包模板！');
                }
            // 第三步
            } else if(this.packageProjectActive === 2){
                if(this.alreadyUploadProjectRes){
                    this.packageProjectActive++;
                } else {
                    window.ZXW_VUE.$message.warning('请先上传项目资源！');
                }
            // 第四步
            } else if(this.packageProjectActive === 3){
                if(this.alreadyHandlerPackageRes){
                    this.packageProjectActive++;
                } else {
                    window.ZXW_VUE.$message.warning('请先处理打包资源！');
                }
            }
        },
        // 上一步
        preSteps(){
            this.packageProjectActive--;
        },
        // 加载自定义签名字典
        loadCustomSignDict(){
            let _that = this;
            this.keyStoreArr = [];
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                data: {
                    "relativeFilePath": 'webCommonPath' + '/' + 'apkPackage' + '/' + 'apkTool'
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                           if(data.data){
                               // 全部的证书数组
                               _that.keyStoreArr = data.data.filter(item=>item.fileType === "keystore").map(item=> item.fileName + "." + item.fileType);
                           }
                        }
                    }
                },
                error: function (msg) {
                }
            });

        },
        // 根据路径加载icon图
        previewIconByPath(iconPath){
            if(!iconPath){
                return;
            }
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            let targetPath = toPath + "/" + iconPath;
            // 关键key
            let dirPathKey = this.deviceInfo.deviceUuid + '_' + targetPath;
            // 更新手机端目录缓存
            let updatePhoneDirCacheFun = () => {
                // 远程执行脚本内容
                let remoteScript = `let result = '';
                let bytes = files.readBytes('${targetPath}');
                let image = images.read('${targetPath}');
                result = images.toBase64(image, "png", 100);
                image.recycle();
                http.request(commonStorage.get("服务端IP") + ':' + (commonStorage.get("服务端Port") || 9998)  +'/attachmentInfo/updateFileMap', {
                    headers: {
                        "deviceUUID": commonStorage.get('deviceUUID')
                    },
                    method: 'POST',
                    contentType: 'application/json',
                    body: JSON.stringify({ 'dirPathKey': commonStorage.get('deviceUUID') + '_' + '${targetPath}', 'fileJson': result })
                }, (e) => { });`;
                this.remoteExecuteScript(remoteScript);
            };
            // 查询缓存数据方法
            queryCacheData(() => {
                $.ajax({
                    url: getContext() + "/attachmentInfo/clearFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                    },
                    error: function (msg) {
                    }
                });
                // 清除完成后执行
                updatePhoneDirCacheFun();
            }, () => {
                let cacheData = null;
                $.ajax({
                    url: getContext() + "/attachmentInfo/queryFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                cacheData = data.data;
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
                return cacheData;
            }, 200, 30,(cacheResultData)=>{
                let fileContent = '';
                this.phoneFileEditorName = '';
                this.phoneImageBase64 = '';
                this.phoneImagePreviewVisible = true;
                this.phoneFileEditorName = iconPath;
                this.phoneImageBase64 = 'data:image/png;base64,' + cacheResultData;
                this.phoneFileLoading = false;
            });
        },
        // 打包须知
        phonePackageProjectTips(){
            let tipsHtml = `
            <div>
                1、打包功能需要购买打包插件后才能使用,请点击顶部打包插件下载查看。<br/>
                2、打包功能需要依赖打包插件,请先初始化插件,具体见公共文件模块。<br/>
                3、打包功能需要机器码授权后才能使用,请购买插件后联系管理员。<br/>
                4、打包功能需要JAVA环境支持,请在公共文件模块设置。<br/>
                5、打包功能需要依赖自定义签名,请在公共文件模块设置。<br/>
                6、打包功能暂不支持直接使用安卓资源的项目。<br/>
                7、打包过程中,若在第一步更改了项目配置或者修改了项目文件,如果需要再次打包,建议每步骤都执行一遍,保证资源更新。<br/>
                8、打包过程中,设置了自定义应用图标和启动界面图标,如果打包报错,可以重新传一个图片后再次尝试,或者修改其后缀名为jpg或者png。<br/>
                9、如有其他问题请在QQ群：806074622中反馈。</div>
            `;
            this.$msgbox({
                title: '打包须知',
                dangerouslyUseHTMLString: true,
                customClass:"messageTipsClass",
                message: tipsHtml,
                confirmButtonText: '确定'
            }).then(action => {
            });
        },
        // 保存初始化配置参数
        saveProjectJsonFun(){
            // 获取临时缓存变量
            let saveProjectJsonObj = JSON.parse(JSON.stringify(this.projectJsonObj));
            saveProjectJsonObj.name = (this.packageProject.appName||"").trim();
            saveProjectJsonObj.packageName = (this.packageProject.packageName||"").trim();
            saveProjectJsonObj.versionName = (this.packageProject.versionName||"").trim();
            saveProjectJsonObj.versionCode = String(this.packageProject.versionCode||"").trim();
            saveProjectJsonObj.icon = (this.packageProject.appIcon||"").trim();
            // NodeJs环境判断
            if(!saveProjectJsonObj.features){
                saveProjectJsonObj.features = {};
            }
            saveProjectJsonObj.features.nodejs = this.packageProject.openNodeJs ? "enabled" : "disabled";

            // 开机自启动
            saveProjectJsonObj.autoOpen = this.packageProject.autoOpen;

            let pluginsMap = {
                "org.autojs.autojspro.plugin.mlkit.ocr": "1.1",
                "org.autojs.autojspro.ocr.v2":"1.3",
                "com.tomato.ocr":"1.0",
                "com.hraps.ocr32":"1.0",
                "com.hraps.ocr":"2.0.0",
                "cn.lzx284.p7zip":"1.2.1",
                "com.hraps.pytorch":"1.0",
                "org.autojs.plugin.ffmpeg":"1.1"
            };
            let plugins = saveProjectJsonObj.plugins || {};
            this.packageProject.plugins.forEach(key=>{
                plugins[key] = pluginsMap[key];
            });
            saveProjectJsonObj.plugins = plugins;

            saveProjectJsonObj.abis = this.packageProject.abis || [];

            if(!saveProjectJsonObj.launchConfig){
                saveProjectJsonObj.launchConfig = {};
            }
            saveProjectJsonObj.launchConfig.hideLogs = this.packageProject.hideLogs;
            saveProjectJsonObj.launchConfig.splashText = this.packageProject.splashText;
            saveProjectJsonObj.launchConfig.splashIcon = this.packageProject.splashIcon;

            if(!saveProjectJsonObj.signingConfig){
                saveProjectJsonObj.signingConfig = {};
            }
            saveProjectJsonObj.signingConfig.keystore = this.packageProject.customSignStorePath;
            // 将json写入本地文件
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            let targetPath = toPath + "/project.json";
            let remoteScript = `let writableTextFile = files.write('${targetPath}',decodeURI($base64.decode('${btoa(encodeURI(JSON.stringify(saveProjectJsonObj,"","\t")))}')));`;
            this.remoteExecuteScript(remoteScript);
        },
        // 初始化配置参数
        initProjectJsonFun(){
            // 初始化配置参数
            this.packageProject.appName = this.projectJsonObj.name;
            this.packageProject.packageName = this.projectJsonObj.packageName;
            this.packageProject.versionName = this.projectJsonObj.versionName;
            this.packageProject.versionCode = String(this.projectJsonObj.versionCode);
            this.packageProject.appIcon = this.projectJsonObj.icon;
                // 开机自启动
            this.packageProject.autoOpen = this.projectJsonObj.autoOpen;

            // NodeJs环境判断
            let features = this.projectJsonObj.features;
            this.packageProject.openNodeJs = features && features.nodejs === "enabled";

            // 插件列表读取
            let pluginsKeys = Object.keys(this.projectJsonObj.plugins || []);
            this.packageProject.plugins = pluginsKeys;

            this.packageProject.abis = this.projectJsonObj.abis || [];

            let launchConfig = this.projectJsonObj.launchConfig ? this.projectJsonObj.launchConfig : {};

            this.packageProject.hideLogs = launchConfig.hideLogs || true;
            this.packageProject.splashText = launchConfig.splashText || "";
            this.packageProject.splashIcon = launchConfig.splashIcon || "";

            let signingConfig = this.projectJsonObj.signingConfig;
            // 自定义签名处理
            this.packageProject.customSignStorePath = signingConfig ? signingConfig.keystore : "";
        },
        // 获取手机端项目json
        getPhoneProjectJson(callback){
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            let targetPath = toPath + "/project.json";
            // 关键key
            let dirPathKey = this.deviceInfo.deviceUuid + '_' + targetPath;
            // 更新手机端目录缓存
            let updatePhoneDirCacheFun = () => {
                // 远程执行脚本内容
                let remoteScript = `let result = '';
                let text = files.read('${targetPath}');
                result = $base64.encode(encodeURI(text));
                http.request(commonStorage.get("服务端IP") + ':' + (commonStorage.get("服务端Port") || 9998)  +'/attachmentInfo/updateFileMap', {
                    headers: {
                        "deviceUUID": commonStorage.get('deviceUUID')
                    },
                    method: 'POST',
                    contentType: 'application/json',
                    body: JSON.stringify({ 'dirPathKey': commonStorage.get('deviceUUID') + '_' + '${targetPath}', 'fileJson': result })
                }, (e) => { });`;
                this.remoteExecuteScript(remoteScript);
            };
            // 查询缓存数据方法
            queryCacheData(() => {
                $.ajax({
                    url: getContext() + "/attachmentInfo/clearFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                    },
                    error: function (msg) {
                    }
                });
                // 清除完成后执行
                updatePhoneDirCacheFun();
            }, () => {
                let cacheData = null;
                $.ajax({
                    url: getContext() + "/attachmentInfo/queryFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                cacheData = data.data;
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
                return cacheData;
            }, 200, 30,(cacheResultData)=>{
                let fileContent = cacheResultData ? decodeURI(atob(cacheResultData)) : '';
                if(callback){
                    callback(fileContent);
                }
            });
        },
        // 检测web端打包模板
        checkWebPackageTemplate(){
            let exits = false;
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                async: false,
                data: {
                    "relativeFilePath": this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/" + this.packageProject.appName
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            if(data.data && data.data.length){
                                exits = true;
                            }
                        }
                    }
                },
                error: function (msg) {
                }
            });
            return exits;
        },
        // 初始化打包模板
        initPackageTemplate(){
            let _that = this;
            this.packageProjectStepLoading = true;
            $.ajax({
                url: getContext() + "/attachmentInfo/initPackageTemplate",
                type: "GET",
                dataType: "json",
                data: {
                    "webProjectRootPath": this.absolutePrePath + this.deviceInfo.deviceUuid + "/" + "apkPackage",
                    "webProjectName": this.packageProject.appName
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            _that.alreadyInitPackageTemplate = data.data;
                            _that.packageProjectStepLoading = false;
                            window.ZXW_VUE.$notify.success({message: '初始化完成', duration: '1000'});
                        } else {
                            _that.packageProjectStepLoading = false;
                            window.ZXW_VUE.$message.warning(data.msg);
                        }
                    } else {
                        _that.packageProjectStepLoading = false;
                    }
                },
                error: function (msg) {
                    _that.packageProjectStepLoading = false;
                }
            });
        },
        // 检测web端项目资源
        checkProjectRes(){
            let exits = false;
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                async: false,
                data: {
                    "relativeFilePath": this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/"
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            if(data.data){
                                let dataArr = data.data.filter(item=> item.pathName.indexOf(_that.packageProject.appName + "_projectRes.zip") !== -1);
                                exits = dataArr.length > 0;
                            }
                        }
                    }
                },
                error: function (msg) {
                }
            });
            return exits;
        },
        // 上传项目资源
        uploadProjectRes(){
            this.packageProjectStepLoading = true;
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            toPath = toPath.replace(/\/$/, "");
            let phoneZipPath =  toPath.substring(0,toPath.lastIndexOf("/")+1) + this.packageProject.appName + '.zip';
            let relativeFilePath = this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/" +this.packageProject.appName + "_projectRes.zip";
            // 文件内容变化后处理函数
            handlerByFileChange(relativeFilePath,()=>{
                let remoteScript = `
                files.remove('${phoneZipPath}')
                $zip.zipDir('${toPath}', '${phoneZipPath}')
                utilsObj.uploadFileToServer('${phoneZipPath}','${relativeFilePath}',()=>{});
                `;
                // 删除本地压缩文件-压缩目标文件夹-上传web端
                this.remoteExecuteScript(remoteScript);
            },()=>{
                this.packageProjectStepLoading = false;
                this.alreadyUploadProjectRes = true;
            });
        },
        // 检测打包资源处理状态
        checkPackageResHandlerStatus(){
            let exits = false;
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                async: false,
                data: {
                    "relativeFilePath": this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/" + this.packageProject.appName
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            if(data.data){
                                let dataArr = data.data.filter(item=> item.pathName.indexOf("packageResAlreadyHandler") !== -1);
                                exits = dataArr.length > 0;
                            }
                        }
                    }
                },
                error: function (msg) {
                }
            });
            return exits;
        },
        // 处理打包资源
        handlerPackageRes(){
            this.packageProjectStepLoading = true;
            // 调用接口
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/handlerPackageProjectRes",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    "webProjectRootPath": this.absolutePrePath + this.deviceInfo.deviceUuid + "/" + "apkPackage",
                    "webProjectName": this.packageProject.appName,
                    "appName":this.packageProject.appName,
                    "packageName":this.packageProject.packageName,
                    "versionName":this.packageProject.versionName,
                    "versionCode":this.packageProject.versionCode,
                    "appIcon":this.packageProject.appIcon,
                    "openNodeJs":this.packageProject.openNodeJs,
                    "autoOpen":this.packageProject.autoOpen,
                    "plugins":this.packageProject.plugins.join(','),
                    "abis":this.packageProject.abis.join(','),
                    "hideLogs":this.packageProject.hideLogs,
                    "splashText":this.packageProject.splashText,
                    "splashIcon":this.packageProject.splashIcon,
                    "customSignAlias":""
                }),
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            if(data.data){
                                _that.alreadyHandlerPackageRes = true;
                            }
                        }
                    }
                    _that.packageProjectStepLoading = false;
                },
                error: function (msg) {
                    _that.packageProjectStepLoading = false;
                }
            });

        },
        // 检测打包项目
        checkPackageProject(){
            let exits = false;
            let _that = this;
            $.ajax({
                url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                type: "GET",
                dataType: "json",
                async: false,
                data: {
                    "relativeFilePath": this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/"
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            if(data.data){
                                let dataArr = data.data.filter(item=> item.pathName.indexOf(_that.packageProject.appName + ".apk") !== -1);
                                exits = dataArr.length > 0;
                            }
                        }
                    }
                },
                error: function (msg) {
                }
            });
            return exits;
        },
        // 获取JavaHome
        getJavaHome(){
            let JAVA_HOME = '';
            let _that = this;
            $.ajax({
                url: getContext() + '/uploadPath/autoJsTools/' + 'webCommonPath' + '/' + 'apkPackage' + '/' + 'apkTool' + '/' + 'JAVA_HOME.json',
                type: 'get',
                async: false,
                dataType:"TEXT", //返回值的类型
                success: function (res) {
                    JAVA_HOME = String(res)
                },
                error: function (msg) {
                }
            });
            return JAVA_HOME;
        },
        // 获取证书信息
        getKeyStoreObjBySelect(){
            let keyStoreObj = {};
            let _that = this;
            let keyStoreFilePath = this.packageProject.customSignStorePath;
            keyStoreFilePath = keyStoreFilePath.replace(".keystore","");
            $.ajax({
                url: getContext() + '/uploadPath/autoJsTools/' + 'webCommonPath' + '/' + 'apkPackage' + '/' + 'apkTool' + '/' + keyStoreFilePath + '.json',
                type: 'get',
                async: false,
                dataType:"TEXT", //返回值的类型
                success: function (res) {
                    let string = String(res);
                    if(string){
                        keyStoreObj = JSON.parse(string);
                    }
                },
                error: function (msg) {
                }
            });
            return keyStoreObj;
        },
        // 处理打包项目
        handlerPackageProject(){
            let _that = this;
            this.packageProjectStepLoading = true;
            // 获取javaHome
            let javaHome = this.getJavaHome();
            // 获取选择证书
            let keyStoreObj = this.getKeyStoreObjBySelect();
            if(!keyStoreObj || !Object.keys(keyStoreObj).length){
                window.ZXW_VUE.$message.warning('未找到签名配置,请先到公共文件生成签名');
                this.packageProjectStepLoading = false;
                return;
            }
            $.ajax({
                url: getContext() + "/attachmentInfo/packageProject",
                type: "GET",
                dataType: "json",
                data: {
                    "javaHome":javaHome,
                    "webProjectRootPath": this.absolutePrePath + this.deviceInfo.deviceUuid + "/" + "apkPackage",
                    "webProjectName": this.packageProject.appName,
                    "keyStoreFile": keyStoreObj ? keyStoreObj["keyStoreFile"] : "",
                    "keyStoreAlias":keyStoreObj ? keyStoreObj["keyStoreAlias"] : "",
                    "keyStorePwd":keyStoreObj ? keyStoreObj["keyStorePwd"] : "",
                    "keyStoreAliasPwd":keyStoreObj ? keyStoreObj["keyStoreAliasPwd"] : ""
                },
                success: function (data) {
                    if (data) {
                        if (data.isSuccess) {
                            let message = data.data;
                            console.log(message);
                            _that.alreadyCompletePackageProject = _that.checkPackageProject();
                            _that.packageProjectStepLoading = false;
                            if(_that.alreadyCompletePackageProject){
                                window.ZXW_VUE.$notify.success({message: '打包成功', duration: '1000'});
                            } else {
                                window.ZXW_VUE.$message.error({message: message, duration: '3000'});
                            }
                        } else {
                            _that.packageProjectStepLoading = false;
                            window.ZXW_VUE.$message.warning(data.msg);
                        }
                    } else {
                        _that.packageProjectStepLoading = false;
                    }
                },
                error: function (msg) {
                    _that.packageProjectStepLoading = false;
                }
            });
        },
        // 下载打包后的文件
        downloadPackageProject(){
            if(!this.alreadyCompletePackageProject){
                window.ZXW_VUE.$message.warning('请先打包项目！');
                return;
            }
            // 创建a标签，通过a标签实现下载
            const dom = document.createElement("a");
            dom.href = getContext() + "/uploadPath/autoJsTools/" + this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/" + this.packageProject.appName + ".apk";
            console.log(dom.href);
            dom.id = "upload-file-dom";
            dom.style.display = "none";
            document.body.appendChild(dom);
            // 触发点击事件
            dom.click();
            document.getElementById("upload-file-dom")?.remove();
        },
        // 下载项目到手机
        downloadPackageProjectToPhone(){
            if(!this.alreadyCompletePackageProject){
                window.ZXW_VUE.$message.warning('请先打包项目！');
                return;
            }
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            toPath = toPath.substring(0,toPath.length - 1);
            let localFileUrl = this.packageProject.appName + ".apk";
            let downloadFilUrl = getContext() + "/uploadPath/autoJsTools/" + this.deviceInfo.deviceUuid + "/" + "apkPackage" + "/" + this.packageProject.appName + ".apk";

            let message = "安装包下载路径为：/sdcard/"+localFileUrl+",请查看!";
            // 创建目录代码 如果不是/ 则需要创建目录
            let script =  "utilsObj.downLoadFile('"+downloadFilUrl+"','"+localFileUrl+"',()=>{app.viewFile('/sdcard/"+localFileUrl+"');});";
            this.remoteExecuteScript(script);
            window.ZXW_VUE.$message.info({
                message: message,
                duration: '2000'
            });
        },
        // 压缩文件
        zipFile(row){
            let path = this.navigatePath.replace('根目录','');
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            window.ZXW_VUE.$prompt('请输入要压缩到的目录及文件名(第一个/表示根目录)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: path + '/' +row.fileName + '.zip',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let _that = this;
                let pathName = this.absolutePrePath + this.deviceInfo.deviceUuid + value;
                $.ajax({
                    url: getContext() + "/attachmentInfo/zipServerFileZip",
                    type: "get",
                    dataType: "json",
                    data: {
                        "sourceFolderPathName": row.absolutePathName,
                        "targetFilePathName": pathName,
                        "zipPathName":""
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '压缩成功', duration: '1000'});
                                // 重新加载文件列表
                                _that.queryFileList(toPath);
                            }
                        }
                    },
                    error: function (msg) {
                        console.log(msg)
                    }
                });
            }).catch(() => {
            });
        },
        // 手机端下载目录
        phoneDownLoadDirectory(row){
            this.phoneFileLoading = true;
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            let phoneZipPath = toPath + row.fileName + '.zip';
            let relativeFilePath = this.deviceInfo.deviceUuid  + '/tempPhoneDownLoad/' + row.fileName + '.zip';
            row.previewUrl = '/uploadPath/autoJsTools/'+relativeFilePath;
            row.fileType = 'zip';
            // 文件内容变化后处理函数
            handlerByFileChange(relativeFilePath,()=>{
                let remoteScript = `
                files.remove('${phoneZipPath}')
                $zip.zipDir('${row.pathName}', '${phoneZipPath}')
                utilsObj.uploadFileToServer('${phoneZipPath}','${relativeFilePath}',()=>{});
                `;
                // 删除本地压缩文件-压缩目标文件夹-上传web端
                this.remoteExecuteScript(remoteScript);
            },()=>{
                this.phoneFileLoading = false;
                this.downloadFile(row)
            });
        },
        // 手机端运行选中脚本 弹窗
        phoneRunScriptByDialog(){
            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
            let savePath = fileObj.fileSavePath;
            if(!savePath){
                return;
            }
            let parentSavePath = savePath.substring(0,this.phoneFileSavePath.lastIndexOf('/'));
            let remoteScript = `engines.execScriptFile("${savePath}",{path:["${parentSavePath}"]})`;
            this.remoteExecuteScript(remoteScript);
        },
        // 手机端停止全部脚本
        phoneStopAllScript(){
            this.phoneRemoteStopScript();
        },
        // 手机端运行文件
        phoneRunScriptPath(row){
            window.ZXW_VUE.$confirm('是否确认在手机端运行脚本【' + row.pathName + '】?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let remoteScript = `engines.execScriptFile("${row.pathName}",{path:["${row.parentPathName}"]})`;
                this.remoteExecuteScript(remoteScript);
            });
        },
        // 手机端下载文件
        phoneDownLoadFile(row){
            this.phoneFileLoading = true;
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            let phonePath = toPath + row.fileName + '.' + row.fileType;
            let relativeFilePath = this.deviceInfo.deviceUuid  + '/tempPhoneDownLoad/' + row.fileName + '.' +row.fileType;
            row.previewUrl = '/uploadPath/autoJsTools/'+relativeFilePath;
            // 文件内容变化后处理函数
            handlerByFileChange(relativeFilePath,()=>{
                let remoteScript = `
                utilsObj.uploadFileToServer('${phonePath}','${relativeFilePath}',()=>{});
                `;
                // 上传web端
                this.remoteExecuteScript(remoteScript);
            },()=>{
                this.phoneFileLoading = false;
                this.downloadFile(row)
            });
        },
        // 手机端一键下载
        phoneOneKeyDownLoad(row){
            let fileNames = (row.isDirectory || !row.fileType) ? row.fileName : row.fileName +'.' + row.fileType;
            window.ZXW_VUE.$confirm('是否确认下载' + fileNames + '?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                if((row.isDirectory || !row.fileType)){
                    this.phoneDownLoadDirectory(row);
                } else {
                    this.phoneDownLoadFile(row);
                }
            });
        },
        // 手机端压缩文件
        phoneZipFile(row){
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            window.ZXW_VUE.$prompt('请输入要压缩到的目录及文件名(根目录是/sdcard/)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: toPath + row.fileName + '.zip',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let remoteScript = `$zip.zipDir('${row.pathName}', '${value}')`;
                this.remoteExecuteScript(remoteScript);
                setTimeout(()=>{
                    // 刷新手机目录
                    this.refreshPhoneDir();
                },500);
            }).catch(() => {
            });
        },
        // 解压文件
        unZipFile(row){
            let path = this.navigatePath.replace('根目录','');
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            window.ZXW_VUE.$prompt('请输入要解压到的目录(第一个/表示根目录)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: path + '/' +row.fileName,
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let _that = this;
                let pathName = this.absolutePrePath + this.deviceInfo.deviceUuid + value;
               $.ajax({
                    url: getContext() + "/attachmentInfo/unServerFileZip",
                    type: "get",
                    dataType: "json",
                    data: {
                        "sourcePathName": row.absolutePathName,
                        "targetPathName": pathName
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '解压成功', duration: '1000'});
                                // 重新加载文件列表
                                _that.queryFileList(toPath);
                            }
                        }
                    },
                    error: function (msg) {
                        console.log(msg)
                    }
                });
            }).catch(() => {
            });
        },
        // 手机端解压文件
        phoneUnZipFile(row){
            let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
            window.ZXW_VUE.$prompt('请输入要解压到的目录(根目录是/sdcard/)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: toPath +row.fileName,
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let remoteScript = `$zip.unzip('${row.pathName}', '${value}')`;
                this.remoteExecuteScript(remoteScript);
                setTimeout(()=>{
                    // 刷新手机目录
                    this.refreshPhoneDir();
                },500);
            }).catch(() => {
            });
        },
        // 关闭编辑器
        closeFileEditorDialog(){
            this.fileEditVisible = false;
        },
        // 关闭确认
        confirmClose(done){
            // 如果存在有修改 未保存的情况 提示保存
            if(this.phoneFileChangeArr.filter(item=>item).length>0){
                window.ZXW_VUE.$confirm('确认关闭编辑器,未保存文件将不会保留更改?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'info'
                }).then(() => {
                    this.phoneCloseFileEditorDialog();
                });
            } else {
                this.phoneCloseFileEditorDialog();
            }
        },
        // 关闭弹窗
        phoneCloseFileEditorDialog(){
            this.phoneFileEditVisible = false;
            this.updateFileDialogIsMin(false);
            this.phoneFileCacheArr = []; // 手机端文件缓存列表
            this.phoneFileSelectIndex = -1
        },
        // 最大化弹窗
        phoneMaxFileEditorDialog(){
            this.updateFileDialogIsMin(false);
            this.phoneFileEditVisible = true;
        },
        // 最小化弹窗
        phoneMinFileEditorDialog(){
            this.updateFileDialogIsMin(true);
            this.phoneFileEditVisible = false;
        },
        phoneCloseImagePreviewDialog(){
            this.phoneImagePreviewVisible = false;
        },
        // 保存文件编辑器内容
        saveFileEditorContent(){
            window.ZXW_VUE.$confirm('确认保存文件【' + this.navigatePath + '/' + this.fileEditorName + '】?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let scriptFile = new File([this.scriptEditor.getValue()], this.fileEditorName, {
                    type: "text/plain",
                });
                const param = new FormData();
                param.append('file', scriptFile);
                param.append('pathName', this.fileSavePath);
                let _that = this;
                $.ajax({
                    url: getContext() + "/attachmentInfo/uploadFileSingle",
                    type: 'post',
                    data: param,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '保存成功', duration: '1000'});
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
            });
        },
        // 手机端保存文件编辑器内容
        phoneSaveFileEditorContent(){
            // 获取当前点击的文件对象
            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
            if(!fileObj){
                window.ZXW_VUE.$message.warning('未获取到文件,请重试！');
                return;
            }
            window.ZXW_VUE.$confirm('确认保存手机端文件【' + fileObj.fileSavePath + '】?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let remoteScript = `let writableTextFile = files.write('${fileObj.fileSavePath}',decodeURI($base64.decode('${btoa(encodeURI(fileObj.fileContent))}')));`;
                this.remoteExecuteScript(remoteScript);
                // 更新原始缓存值
                fileObj.sourceFileContent = fileObj.fileContent;
            });
        },
        // 面包屑change
        breadcrumbChange(item, index) {
            if (!this.validSelectDevice()) {
                return;
            }
            // 加载文件列表
            this.queryFileList(item.value);
            // 重新加载面包屑
            this.breadcrumbList = this.breadcrumbList.slice(0, index + 1);
            this.$set(this.breadcrumbList, 0, {label: '根目录', value: this.deviceInfo.deviceUuid});

            if(this.autoSyncWebSyncPath){
                let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                let replacePath = toPath.replace(this.deviceInfo.deviceUuid,"");
                this.webSyncPath =  replacePath + "/";
            }
        },
        // 面包屑导航
        phoneBreadcrumbChange(item, index){
            if (!this.validSelectDevice()) {
                return;
            }
            this.updatePhoneDirCache(item.value);
            // 重新加载面包屑
            this.phoneBreadcrumbList = this.phoneBreadcrumbList.slice(0, index + 1);
            if(this.autoSyncPhoneSyncPath){
                let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
                this.phoneSyncPath =  toPath.replace("/sdcard", "");
            }
        },
        // 刷新手机端路径
        refreshPhoneDir(){
            // 默认加载最后一个
            this.phoneBreadcrumbChange(this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1], (this.phoneBreadcrumbList.length - 1))
        },
        // 刷新web端目录
        refreshWebDir(){
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            this.queryFileList(toPath);
        },
        // 计算文件大小
        calculateSize(fileSize) {
            let gb = 1024 * 1024 * 1024;
            let mb = 1024 * 1024;
            let kb = 1024;
            let val = parseFloat(fileSize / gb).toFixed(2);
            if (val > 1) {
                return val + "GB";
            }
            val = parseFloat(fileSize / mb).toFixed(2);
            if (val > 1) {
                return val + "MB";
            }
            val = parseFloat(fileSize / kb).toFixed(2);
            if (val > 1) {
                return val + "KB";
            }
            return fileSize + "B";
        },
        // 校验同步参数
        validateSyncParam(){
            if (!this.validSelectDevice()) {
                return false;
            }
            if (!this.webSyncPath) {
                window.ZXW_VUE.$message.warning('请设置web端同步路径');
                return false;
            }
            if(!this.webSyncPath.startsWith("/")){
                window.ZXW_VUE.$message.warning("web端同步路径必须以/开头");
                return false;
            }
            if(!this.webSyncPath.endsWith("/")){
                window.ZXW_VUE.$message.warning("web端同步路径必须以/结尾");
                return false;
            }
            if (!this.phoneSyncPath) {
                window.ZXW_VUE.$message.warning('请设置手机端同步路径');
                return false;
            }
            if (this.phoneSyncPath.startsWith("/sdcard")) {
                window.ZXW_VUE.$message.warning('手机端同步路径无需以/sdcard开头');
                return false;
            }
            if (!this.phoneSyncPath.startsWith("/")) {
                window.ZXW_VUE.$message.warning("手机端同步路径必须以/开头");
                return false;
            }
            if (!this.phoneSyncPath.endsWith("/")) {
                window.ZXW_VUE.$message.warning("手机端同步路径必须以/开头");
                return false;
            }
            return true;
        },
        // 同步文件到手机端
        syncFileToPhone() {
            if (!this.validateSyncParam()) {
                return;
            }
            window.ZXW_VUE.$confirm('是否确认将web端【根目录'+this.webSyncPath+'】目录文件(文件夹不会递归同步),同步到手机端【' + this.phoneSyncPath + '】下?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let _that = this;
                let relativeFilePath = this.deviceInfo.deviceUuid + this.webSyncPath;
                $.ajax({
                    url: getContext() + "/attachmentInfo/queryAttachInfoListByPath",
                    type: "GET",
                    dataType: "json",
                    // contentType: "application/json",
                    data: {
                        "relativeFilePath": relativeFilePath
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                let webSyncToPhoneArr = data.data;
                                _that.webSyncToPhoneFun(webSyncToPhoneArr,_that.phoneSyncPath);
                            }
                        }
                        setTimeout(() => {
                        }, 200)
                    },
                    error: function (msg) {
                    }
                });
            });
        },
        // 同步文件到Web端
        syncFileToWeb() {
            if (!this.validateSyncParam()) {
                return;
            }
            window.ZXW_VUE.$confirm('是否确认将手机端【' + this.phoneSyncPath + '】目录文件(文件夹不会递归同步),同步到web端【根目录'+this.webSyncPath+'】下?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                // 定义根目录
                let remoteExecScriptContent = 'let dir = "/sdcard'+ this.phoneSyncPath+'";\r\n';
                // 定义web端路径
                remoteExecScriptContent += 'let webPath = utilsObj.getDeviceUUID()+ "'+ this.webSyncPath+'";\r\n';
                // 读取全部的文件
                remoteExecScriptContent += 'let allList = files.listDir(dir);\r\n';
                // 遍历调用上传方法
                remoteExecScriptContent += 'allList.forEach(name=>{ \r\n';
                remoteExecScriptContent +=  '  let fileName = webPath+name;\r\n';
                remoteExecScriptContent +=  '  let localFilePath = dir+name;\r\n';
                remoteExecScriptContent +=  '  if(files.isDir(localFilePath)){\r\n';
                remoteExecScriptContent +=  '     utilsObj.request("/attachmentInfo/createFolder?folderName="+fileName,"GET",null,()=>{});\r\n';
                remoteExecScriptContent +=  '  }else{\r\n';
                remoteExecScriptContent +=  '     utilsObj.uploadFileToServer(localFilePath,fileName,()=>{console.log(localFilePath+"上传完成")});\r\n';
                remoteExecScriptContent +=  '  }\r\n';
                remoteExecScriptContent += '})\r\n';
                remoteExecScriptContent += 'toastLog("上传完成,一共"+allList.length+"个文件")\r\n';
                this.remoteExecuteScript(remoteExecScriptContent);
                setTimeout(()=>{
                    window.ZXW_VUE.$notify.success({message: '请手动刷新目录', duration: '1000'});
                },1000)
            });
        },
        // 重命名
        reName(row){
            window.ZXW_VUE.$prompt('请输入新的名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let _that = this;
                let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                let replacePath = toPath.replace(/\//g, "\\");
                $.ajax({
                    url: getContext() + "/attachmentInfo/reNameFile",
                    type: "GET",
                    dataType: "json",
                    data: {
                        "oldFilePathName":row.pathName,
                        "newFilePathName": _that.absolutePrePath + replacePath + "\\" + value
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '重命名成功', duration: '1000'});
                                // 重新加载文件列表
                                _that.queryFileList(toPath);
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
            }).catch(() => {
            });
        },
        // 手机端重命名
        phoneReName(row){
            window.ZXW_VUE.$prompt('请输入新的名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let remoteScript = `files.rename('${row.pathName}', '${value}')`;
                this.remoteExecuteScript(remoteScript);
                // 刷新手机目录
                this.refreshPhoneDir();
            }).catch(() => {
            });

        },
        // 删除单个文件
        removeFile(row){
            let fileNames =  (row.isDirectory || !row.fileType) ? row.fileName : (row.fileName + "." + row.fileType);
            let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
            window.ZXW_VUE.$confirm('是否确认删除' + fileNames + '?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let _that = this;
                $.ajax({
                    url: getContext() + "/attachmentInfo/deleteFile",
                    type: "GET",
                    dataType: "json",
                    data: {
                        "filePath":row.pathName
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '删除成功', duration: '1000'});
                                // 重新加载文件列表
                                _that.queryFileList(toPath);
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
            });
        },
        // 手机删除单个文件
        phoneRemoveFile(row){
            let fileNames =  (row.isDirectory || !row.fileType) ? row.fileName : (row.fileName + "." + row.fileType);
            window.ZXW_VUE.$confirm('是否确认删除' + fileNames + '?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let remoteScript = row.isDirectory ? `files.removeDir('${row.pathName}')` : `files.remove('${row.pathName}')`;
                this.remoteExecuteScript(remoteScript);
                // 刷新手机目录
                this.refreshPhoneDir();
            });
        },
        // 单个文件同步到手机
        syncToPhoneSingle(row){
            window.ZXW_VUE.$prompt('请输入手机端路径(以/sdcard为根目录的相对路径)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: this.phoneSyncPath,
                inputValidator: function(val) {
                    if(val){
                        if(val.startsWith("/sdcard")){
                            return "无需以/sdcard开头"
                        }
                        if(!val.startsWith("/")){
                            return "必须以/开头"
                        }
                        if(!val.endsWith("/")){
                            return "必须以/结尾"
                        }
                    } else {
                        return "不能为空";
                    }
                    return true;
                }
            }).then(({value}) => {
                let webSyncToPhoneArr = [row];
                this.webSyncToPhoneFun(webSyncToPhoneArr,value);
            }).catch(() => {
            });
        },
        // 创建文件夹
        createFolder() {
            if (!this.validSelectDevice()) {
                return;
            }
            window.ZXW_VUE.$prompt('请输入文件夹名称', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let _that = this;
                let toPath = this.breadcrumbList[this.breadcrumbList.length - 1].value;
                let replacePath = toPath.replace(/\//g, "\\");
                $.ajax({
                    url: getContext() + "/attachmentInfo/createFolder",
                    type: "GET",
                    dataType: "json",
                    data: {
                        "folderName": replacePath + "\\" + value
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                window.ZXW_VUE.$notify.success({message: '新建成功', duration: '1000'});
                                // 重新加载文件列表
                                _that.queryFileList(toPath);
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
            }).catch(() => {
            });
        },
        // 新建文件(夹)
        phoneCreateFile(){
            if (!this.validSelectDevice()) {
                return;
            }
            window.ZXW_VUE.$prompt('请输入文件(夹)名称(文件夹请以/结尾)', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let toPath = this.phoneBreadcrumbList[this.phoneBreadcrumbList.length - 1].value;
                let remoteScript = '';
                    remoteScript += `files.createWithDirs('${toPath}'+'${value}')`;
                this.remoteExecuteScript(remoteScript);
                setTimeout(()=>{
                    // 刷新手机目录
                    this.refreshPhoneDir();
                },500);
            }).catch(() => {
            });
        },
        // 手机端初始化同步目录
        phoneInitSyncDir(){
            let remoteScript = `if(files.isFile("/sdcard/appSync")){files.remove("/sdcard/appSync")}; files.createWithDirs("/sdcard/appSync/");toastLog("初始化同步目录完成");`;
            this.remoteExecuteScript(remoteScript);
            this.phoneBreadcrumbList = [{label: '根目录', value: '/sdcard/'},{label: 'appSync', value: '/sdcard/appSync/'}];
            setTimeout(()=>{
                // 刷新手机目录
                this.refreshPhoneDir();
            },500);
        },
        // 初始官方示例
        phoneInitOfficialExample(){
            this.phoneFileLoading = true;
            // 手机端下载官方示例 并且zip解压完成后 web端刷新手机目录
            handlerAppByCacheChange(this.deviceInfo.deviceUuid+"_"+"unzipFinishedExample",()=>{
                let downLoadGameScript = `if(files.isFile("/sdcard/appSync")){files.remove("/sdcard/appSync")}; files.createWithDirs("/sdcard/appSync/");
                utilsObj.downLoadFile("${getContext()}/AutoJsPro官方示例.zip","/appSync/AutoJsPro官方示例.zip",()=>{
                    $zip.unzip('/sdcard/appSync/AutoJsPro官方示例.zip', '/sdcard/appSync/');
                    let finishMsgObj = {
                        "deviceUUID":"${this.deviceInfo.deviceUuid}",
                        "serviceKey":"unzipFinishedExample",
                        "serviceValue":"true"
                    }
                    events.broadcast.emit("sendMsgToWebUpdateServiceKey", JSON.stringify(finishMsgObj));
                    toastLog("初始化AutoJsPro官方示例完成");
                })`;
                this.remoteExecuteScript(downLoadGameScript);
            },()=>{
                this.phoneFileLoading = false;
                this.phoneBreadcrumbList = [{label: '根目录', value: '/sdcard/'},{label: 'appSync', value: '/sdcard/appSync/'}];
                // 刷新手机目录
                this.refreshPhoneDir();
            });
        },
		// 初始化官方商店示例
		phoneInitOfficalShopExample(){
			 this.phoneFileLoading = true;
            // 手机端下载官方示例 并且zip解压完成后 web端刷新手机目录
            handlerAppByCacheChange(this.deviceInfo.deviceUuid+"_"+"unzipFinishedShopExample",()=>{
                let downLoadGameScript = `if(files.isFile("/sdcard/appSync")){files.remove("/sdcard/appSync")}; files.createWithDirs("/sdcard/appSync/");
                utilsObj.downLoadFile("${getContext()}/AutoJsPro商店示例脚本.zip","/appSync/AutoJsPro商店示例脚本.zip",()=>{
                    $zip.unzip('/sdcard/appSync/AutoJsPro商店示例脚本.zip', '/sdcard/appSync/');
                    let finishMsgObj = {
                        "deviceUUID":"${this.deviceInfo.deviceUuid}",
                        "serviceKey":"unzipFinishedShopExample",
                        "serviceValue":"true"
                    }
                    events.broadcast.emit("sendMsgToWebUpdateServiceKey", JSON.stringify(finishMsgObj));
                    toastLog("初始化AutoJsPro商店示例脚本完成");
                })`;
                this.remoteExecuteScript(downLoadGameScript);
            },()=>{
                this.phoneFileLoading = false;
                this.phoneBreadcrumbList = [{label: '根目录', value: '/sdcard/'},{label: 'appSync', value: '/sdcard/appSync/'}];
                // 刷新手机目录
                this.refreshPhoneDir();
            });
		},
        // 手机端下载脚手架项目
        phoneDownLoadGameScript(){
            this.phoneFileLoading = true;
            // 手机端下载脚手架项目 并且zip解压完成后 web端刷新手机目录
            handlerAppByCacheChange(this.deviceInfo.deviceUuid+"_"+"unzipFinished",()=>{
                let downLoadGameScript = `if(files.isFile("/sdcard/appSync")){files.remove("/sdcard/appSync")}; files.createWithDirs("/sdcard/appSync/");
                utilsObj.downLoadFile("${getContext()}/hz_autojs_game_script.zip","/appSync/hz_autojs_game_script.zip",()=>{
                    $zip.unzip('/sdcard/appSync/hz_autojs_game_script.zip', '/sdcard/appSync/');
                    let finishMsgObj = {
                        "deviceUUID":"${this.deviceInfo.deviceUuid}",
                        "serviceKey":"unzipFinished",
                        "serviceValue":"true"
                    }
                    events.broadcast.emit("sendMsgToWebUpdateServiceKey", JSON.stringify(finishMsgObj));
                    toastLog("初始化脚手架项目完成");
                })`;
                this.remoteExecuteScript(downLoadGameScript);
            },()=>{
                this.phoneFileLoading = false;
                this.phoneBreadcrumbList = [{label: '根目录', value: '/sdcard/'},{label: 'appSync', value: '/sdcard/appSync/'}];
                // 刷新手机目录
                this.refreshPhoneDir();
            });
        },
        // 手机端初始化运行文件
        phoneInitFile(){
            let initRemoteScript = 'engines.execScriptFile("/sdcard/appSync/hz_autojs_game_script/main.js",{path:["/sdcard/appSync/hz_autojs_game_script"]})';
            window.ZXW_VUE.$prompt('是否确认初始化【/sdcard/appSync/main.js】文件内容如下：', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputValue: initRemoteScript,
                inputValidator: function(val) {
                    if (!val) {
                        return '不能为空！'
                    } else {
                        return true;
                    }
                }
            }).then(({value}) => {
                let remoteScript = `files.createWithDirs('/sdcard/appSync/main.js');`;
                    remoteScript += `files.write('/sdcard/appSync/main.js', '${value}');toastLog("初始化运行程序完成");`;
                    this.remoteExecuteScript(remoteScript);
                    this.phoneBreadcrumbList = [{label: '根目录', value: '/sdcard/'},{label: 'appSync', value: '/sdcard/appSync/'}];
                    setTimeout(()=>{
                        // 刷新手机目录
                        this.refreshPhoneDir();
                    },500);
            }).catch(() => {
            });
        },
        // 手机端远程运行程序
        phoneRemoteRunScript(){
            let remoteScript = `engines.execScriptFile("/sdcard/appSync/main.js");`;
            this.remoteExecuteScript(remoteScript);
        },
        // 手机端远程停止程序
        phoneRemoteStopScript(){
            let remoteScript = `let notCloseSourceArr = ['/data/user/0/com.zjh336.cn.tools/files/project/runScript.js', '/data/user/0/com.zjh336.cn.tools/files/project/main.js']
                                const all = engines.all()
                                all.forEach(item => {
                                    if (!notCloseSourceArr.includes(String(item.source))) {
                                        item.forceStop()
                                    }
                                });`;
            this.remoteExecuteScript(remoteScript);
        },
        // 预览文件
        previewFile(previewUrl) {
            window.open(getContext() + "/" + previewUrl)
        },
        // 下载文件
        downloadFile(row){
            // 创建a标签，通过a标签实现下载
            const dom = document.createElement("a");
            dom.download = row.fileName + '.' + row.fileType;
            dom.href = getContext() +  "/" + row.previewUrl;
            dom.id = "upload-file-dom";
            dom.style.display = "none";
            document.body.appendChild(dom);
            // 触发点击事件
            dom.click();
            document.getElementById("upload-file-dom")?.remove();
        },
        // 更新手机端目录缓存
        updatePhoneDirCache(targetPath) {
            this.phoneFileLoading = false;
            // 关键key
            let dirPathKey = this.deviceInfo.deviceUuid + '_' + targetPath;
            // 更新手机端目录缓存
            let updatePhoneDirCacheFun = () => {
                // 远程执行脚本内容
                let remoteScript = `let targetPath = '${targetPath}'
                function convertFile(item, baseUrl) {
                    let fileObj = {};
                    fileObj.pathName = baseUrl + item
                    fileObj.parentPathName = baseUrl
                    fileObj.isDirectory = files.isDir(fileObj.pathName)
                    fileObj.fileName = item
                    if(!fileObj.isDirectory){
                        if(item.lastIndexOf('.')!==-1){
                            fileObj.fileName = item.substring(0,item.lastIndexOf('.'))
                            fileObj.fileType = item.substring(item.lastIndexOf('.')+1,item.length)
                        }
                    }
                    return fileObj;
                }
                function getFileByPath(filePath) {
                    let fileArr = files.listDir(filePath);
                    let fileList = [];
                    fileArr.forEach(item => {
                        let fileObj = convertFile(item, filePath);
                        if (fileObj.isDirectory) {
                            //let childrenFileList = getFileByPath(fileObj.filePath);
                            //fileObj.children = childrenFileList;
                        }
                        fileList.push(fileObj);
                    })
                    return fileList;
                }
                let appSyncFileList = getFileByPath(targetPath);
                // url编码base64加密
                let result = $base64.encode(encodeURI(JSON.stringify(appSyncFileList)));
                http.request(commonStorage.get("服务端IP") + ':' + (commonStorage.get("服务端Port") || 9998)  +'/attachmentInfo/updateFileDirectoryMap', {
                    headers: {
                        "deviceUUID": commonStorage.get('deviceUUID')
                    },
                    method: 'POST',
                    contentType: 'application/json',
                    body: JSON.stringify({ 'dirPathKey': commonStorage.get('deviceUUID') + '_' + targetPath, 'fileDirectoryJson': result })
                }, (e) => { });`;
                this.remoteExecuteScript(remoteScript);
            };
            this.phoneFileLoading = true;
            // 查询缓存数据方法
            queryCacheData(() => {
                $.ajax({
                    url: getContext() + "/attachmentInfo/clearFileDirectoryMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                    },
                    error: function (msg) {
                    }
                });
                // 清除完成后执行
                updatePhoneDirCacheFun();
            }, () => {
                let cacheData = null;
                $.ajax({
                    url: getContext() + "/attachmentInfo/queryFileDirectory",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                cacheData = data.data;
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
                return cacheData;
            }, 200, 30,(cacheResultData)=>{
                let fileAllArr = cacheResultData ? JSON.parse(decodeURI(atob(cacheResultData))) : [];
                let allArr = [];
                // 目录
                let dirArr = fileAllArr.filter(item=>item.isDirectory).sort((item1, item2) => {
                    let a=item1.fileName||'';
                    let b=item2.fileName||'';
                    for(let i=0; i<a.length;i++){
                        if(a.charCodeAt(i)===b.charCodeAt(i)) continue;
                        return a.charCodeAt(i) - b.charCodeAt(i);
                    }
                });
                // 文件
                let fileArr = fileAllArr.filter(item=>!item.isDirectory).sort((item1, item2) => {
                    let a=item1.fileName||'';
                    let b=item2.fileName||'';
                    for(let i=0; i<a.length;i++){
                        if(a.charCodeAt(i)===b.charCodeAt(i)) continue;
                        return a.charCodeAt(i) - b.charCodeAt(i);
                    }
                });
                allArr = allArr.concat(dirArr);
                allArr = allArr.concat(fileArr);

                let pathNameArr = this.phoneCopyFileList.map(item => item.pathName);
                let pathNameArr2 = this.phoneMoveFileList.map(item => item.pathName);
                allArr.forEach(item => {
                    item.check = pathNameArr.includes(item.pathName) || pathNameArr2.includes(item.pathName) || false
                });
                this.phoneFileList = allArr;
                this.phoneFileTableRandomKey = Math.random();
                this.phoneFileLoading = false;
            });
        },
        // 手机文件编辑器数组点击
        phoneFileEditorArrClick(index){
            // 切换记录索引
            this.phoneFileSelectIndex = index;
            // 获取当前点击的文件对象
            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
            // 设置默认值
            if(!fileObj.scroll){
                fileObj.scroll = {
                    scrollLeft:0,
                    scrollTop:0
                }
            }
            // 缓存的记录
            let cacheScroll = JSON.parse(JSON.stringify(fileObj.scroll));
            // 重置文本编辑器内容
            this.phoneScriptEditor.setValue(fileObj.fileContent || '');
            // 滚动到记录位置
            if(getEditorType() === 'ace'){
                this.phoneScriptEditor.clearSelection();
                this.phoneScriptEditor.session.setScrollTop(cacheScroll.scrollTop);
            } else {
                // 滚动到记录位置
                this.phoneScriptEditor.setScrollPosition(cacheScroll,1);
            }

        },
        // 手机文件编辑器关闭文件缓存
        closePhoneEditorArrClick(index){
            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
            // 关闭之前记录当前文件路径
            let curFileSavePath = fileObj ? fileObj.fileSavePath : '';

            // 公共方法
            let commonFun = ()=>{
                // 清除当前项缓存
                this.phoneFileCacheArr.splice(index, 1);
                // 最后一个关闭，直接关闭弹窗
                if(this.phoneFileCacheArr.length === 0){
                    this.phoneCloseFileEditorDialog();
                    return;
                }
                // 获取文件路径数组
                let fileSavePathArr = this.phoneFileCacheArr.map(item=>item.fileSavePath);
                // 当前文件索引
                let curExitIndex = fileSavePathArr.indexOf(curFileSavePath);

                // 未找到索引 说明当前关闭的就是选中文件
                if(curExitIndex === -1){
                    // 索引位置不变，如果索引位置超出长度则取最后一个
                    // 当前索引项大于等于长度
                    if(this.phoneFileSelectIndex >= this.phoneFileCacheArr.length){
                        // 重置为最后一项
                        this.phoneFileSelectIndex = this.phoneFileCacheArr.length - 1;
                    }
                    // 找到索引，替换选中值
                } else {
                    this.phoneFileSelectIndex = curExitIndex;
                }
                // 最后重新赋值编辑器内容
                fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
                if(fileObj){
                    // 设置默认值
                    if(!fileObj.scroll){
                        fileObj.scroll = {
                            scrollLeft:0,
                            scrollTop:0
                        }
                    }
                    // 缓存的记录
                    let cacheScroll = JSON.parse(JSON.stringify(fileObj.scroll));
                    // 重置文本编辑器内容
                    this.phoneScriptEditor.setValue(fileObj.fileContent || '');
                    if(getEditorType() === 'ace'){
                        this.phoneScriptEditor.clearSelection();
                        this.phoneScriptEditor.session.setScrollTop(cacheScroll.scrollTop);
                    } else {
                        // 滚动到记录位置
                        this.phoneScriptEditor.setScrollPosition(cacheScroll, 1);
                    }
                }
            };

            // 获取当前关闭的项是否存未保存的情况
            let isNotSave = this.phoneFileChangeArr[index];
            // 未保存的要提示，已保存的直接关闭
            if(isNotSave){
                window.ZXW_VUE.$confirm('当前文件未保存，是否确认关闭?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'info'
                }).then(() => {
                    commonFun()
                });
            } else {
                commonFun();
            }
        },
        // 更新手机端文件缓存
        updatePhoneFileCache(row) {
            let targetPath = row.pathName;
            let fileType = row.fileType || '';
            let isImage = ['png','jpg','jpeg'].includes(fileType);
            let isText = ['js','json','css','txt','log','md',''].includes(fileType);
            // 关键key
            let dirPathKey = this.deviceInfo.deviceUuid + '_' + targetPath;
            // 更新手机端目录缓存
            let updatePhoneDirCacheFun = () => {
                // 远程执行脚本内容
                let remoteScript = `let result = '';
                if(${isImage}){
                   let bytes = files.readBytes('${targetPath}');
                   let image = images.read('${targetPath}');
                   result = images.toBase64(image, "png", 100);
                   image.recycle();
                }
                if(${isText}){
                   let text = files.read('${targetPath}');
                   result = $base64.encode(encodeURI(text));
                }
                http.request(commonStorage.get("服务端IP") + ':' + (commonStorage.get("服务端Port") || 9998)  +'/attachmentInfo/updateFileMap', {
                    headers: {
                        "deviceUUID": commonStorage.get('deviceUUID')
                    },
                    method: 'POST',
                    contentType: 'application/json',
                    body: JSON.stringify({ 'dirPathKey': commonStorage.get('deviceUUID') + '_' + '${targetPath}', 'fileJson': result })
                }, (e) => { });`;
                this.remoteExecuteScript(remoteScript);
            };
            // 查询缓存数据方法
            queryCacheData(() => {
                $.ajax({
                    url: getContext() + "/attachmentInfo/clearFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                    },
                    error: function (msg) {
                    }
                });
                // 清除完成后执行
                updatePhoneDirCacheFun();
            }, () => {
                let cacheData = null;
                $.ajax({
                    url: getContext() + "/attachmentInfo/queryFileMap",
                    type: "GET",
                    dataType: "json",
                    async: false,
                    data: {
                        "dirPathKey": dirPathKey
                    },
                    success: function (data) {
                        if (data) {
                            if (data.isSuccess) {
                                cacheData = data.data;
                            }
                        }
                    },
                    error: function (msg) {
                    }
                });
                return cacheData;
            }, 200, 30,(cacheResultData)=>{
                let fileContent = '';
                this.phoneFileEditorName = '';
                this.phoneImageBase64 = '';
                if(isText){
                    this.phoneFileEditVisible = true;
                    fileContent = cacheResultData ? decodeURI(atob(cacheResultData)) : '';
                    this.phoneFileEditorName = '文本编辑';
                    this.phoneFileSavePath = row.pathName;

                    // 获取保存路径的数组
                    let fileSavePathArr = this.phoneFileCacheArr.map(item=>item.fileSavePath);

                    // 当前文件在已有文件列表中的下标
                    let curExitsIndex = fileSavePathArr.indexOf(row.pathName);
                    // 当前文件已打开
                    if(curExitsIndex!==-1){
                        // 显示编辑器 切换索引位置即可
                        this.phoneFileSelectIndex = curExitsIndex;
                        let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
                        if(fileObj){
                            this.phoneScriptEditor.setValue(fileObj.fileContent || '');
                            if(!fileObj.scroll){
                                fileObj.scroll = {
                                    scrollLeft: 0,
                                    scrollTop: 0
                                }
                            }
                            if(this.phoneScriptEditor){
                                // 滚动到记录位置
                                if(getEditorType() === 'ace'){
                                    this.phoneScriptEditor.clearSelection();
                                    this.phoneScriptEditor.session.setScrollTop(fileObj.scroll.scrollTop);
                                } else {
                                    this.phoneScriptEditor.setScrollPosition(fileObj.scroll,1);
                                }
                            }
                        }
                    } else {
                        let fileObj = {
                            fileSavePath:row.pathName,
                            fileName: row.fileName + (row.fileType ? ('.' + row.fileType) : ''),
                            sourceFileContent: fileContent,
                            fileContent: fileContent
                        };
                        this.phoneFileCacheArr.push(fileObj);
                        // 记录索引
                        this.phoneFileSelectIndex = this.phoneFileCacheArr.length - 1;
                        // 初始化文件编辑器
                        initFileEditor(this,'phoneScriptEditor','phoneFileEditor',this.getMonacoEditorComplete,fileContent,'javascript','vs-dark',(e,value)=>{
                            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
                            if(fileObj && fileObj.fileContent!==undefined){
                                this.phoneFileCacheArr[this.phoneFileSelectIndex].fileContent = value;
                            }
                        },(e)=>{
                            let fileObj = this.phoneFileCacheArr[this.phoneFileSelectIndex];
                            if(fileObj){
                                this.phoneFileCacheArr[this.phoneFileSelectIndex].scroll = {
                                    scrollLeft: e.scrollLeft,
                                    scrollTop: e.scrollTop
                                };
                            }
                        });
                        if(this.phoneScriptEditor){
                            if(getEditorType() === 'ace'){
                                this.phoneScriptEditor.session.setScrollTop(0);
                            }else {
                                // 滚动到记录位置
                                this.phoneScriptEditor.setScrollPosition({scrollLeft: 0,scrollTop: 0},1);
                            }
                        }
                    }
                }
                if(isImage){
                    this.phoneImagePreviewVisible = true;
                    this.phoneFileEditorName = row.fileName + (row.fileType ? ('.' + row.fileType) : '');
                    this.phoneImageBase64 = 'data:image/png;base64,' + cacheResultData;
                }
                this.phoneFileLoading = false;
            });
        },
        // 手机端华仔autoJs工具箱热更新
        phoneAutoJsToolHotUpdate(){
            window.ZXW_VUE.$confirm('是否确认远程更新华仔AutoJs工具箱APP端?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'info'
            }).then(() => {
                let remoteScript = `
                // 获取项目路径
                let projectPath = files.cwd();
                // 设置本地临时更新路径
                let tempUpdatePath = "/sdcard/appSync/tempUpdateTools/";
                // 创建临时更新js目录
                files.createWithDirs(tempUpdatePath)
                // 获取热更新版本
                let getResult = http.get("https://gitee.com/zjh336/hz_autojs_toolbox/raw/master/hotUpdateVerson.txt");
                // 可自定义 主要为了app端区分版本
                let hotUpdateVersion = getResult && getResult.statusCode == 200 ? getResult.body.string() : "热更新";
                // 下载工具箱js代码 也可以选择将修改后的js代码 替换到web端上传路径下
                let url = "http://121.4.241.250:9998/uploadPath/autoJsTools/hz_autojs_tools.zip"
                // 请求压缩包
                let r = http.get(url);
                if (r.statusCode == 200) {
                    // 下载压缩包到本地临时更新路径
                    var content = r.body.bytes();
                    files.writeBytes(tempUpdatePath + "hz_autojs_tools.zip", content);
                    // 解压下载文件到 项目路径
                    $zip.unzip(tempUpdatePath + "hz_autojs_tools.zip", projectPath);
                    commonStorage.put("hotUpdateVersion", hotUpdateVersion)
                    toastLog("热更新成功,请重启APP后生效！");
                } else {
                    toastLog(url + "下载失败！！！");
                }`;
                // 执行热更新逻辑
                this.remoteExecuteScript(remoteScript);
            });
        }
    }
}
# 华仔AutoJs工具箱_Web端

#### 介绍
《华仔AutoJs工具箱》web端

[Web端操作文档](https://gitee.com/zjh336/hz_autojs_toolbox_web/wikis/pages)

![输入图片说明](%E5%8D%8E%E4%BB%94AutoJs%E5%B7%A5%E5%85%B7%E7%AE%B1%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B%E9%87%8D%E7%BD%AE%E7%89%88.png)


[《华仔AutoJs工具箱》图色(控件)脚本的好帮手](https://www.zjh336.cn/?id=2109)

![输入图片说明](%E8%B5%9E%E8%B5%8F.png)

#### 软件架构
JAVA8+springboot
